# zetk

<img alt="zk-sim demo" width="31%" src=".doc/sim.svg"/>
<img alt="zk-dot demo" width="31%" src=".doc/graph.svg"/>
<img alt="zk-tree demo" width="31%" src=".doc/tree.svg"/>

Zettelkasten notekeeping library and Unix-like CLI utilities, in C++.
These utilities are best used fronted with a [set of shell
scripts](https://gitlab.com/andrejr/zetk_fzf) that provide a nicer
interface.

## What?

[Zettelkasten](https://en.wikipedia.org/wiki/Zettelkasten) is a simple
note-taking system, where succinct notes (with
thoughts/ideas/concepts/facts) are connected to each other with
cross-references (links) and tags.

Zetk provides a C++ library and a set of CLI tools for navigating,
querying and inspecting your Zettelkasten notes (in Markdown). It
includes utilities for fulltext search, inspecting tags in files,
finding links and backlinks, finding similar documents, finding related
tags, printing link and tag trees and generating
[graphviz](https://graphviz.org/) [dot
language](https://graphviz.org/doc/info/lang.html) graphs representing
the link and tag connections.

On the commandline, zetk is just a set of low-level utilities - I use a
[set of shell scripts](https://gitlab.com/andrejr/zetk_fzf) to wrap
these utilities with [fzf](https://github.com/junegunn/fzf),
[bat](https://github.com/sharkdp/bat),
[tmux](https://github.com/tmux/tmux), for a quick and intuitive
interface.

I plan to reuse the library as basis of a web-based (self-hosted,
backend in C++, Svelte frontend) service which provides all of this
functionality (and Markdown rendering) in the browser, so that you can
access your knowledge base from your phone (read-only).

## Why?

Like [Simon Eskildsen](https://github.com/sirupsen), whose
[zk](https://github.com/sirupsen/zk) inspired this project, I also
wanted a set of tools that work on a plain-text (Markdown) zettelkasten
document collection. I also prefer working in shell, and writing notes
in (Neo)Vim, so I wrote a set of Unix-like utilities that are easy to
script. A zettelkasten's usefulness depends on your ability to
browse/search it, and also your ability to create meaningful connections
while authoring notes, which later facilitates easier browsing. Zetk
provides both kinds of tools.

The value proposal for this set of tools is that they are fast, parallel
(multi-threaded), with SQLite3's
[fts5](https://www.sqlite.org/fts5.html) doing the heavy lifting for
full-text search and indexing. The CLI interface is consistent and
modeled after GNU coreutils.

## Building and installation

If you're an Arch Linux user, you can install the
[zetk](https://aur.archlinux.org/packages/zetk) package from AUR.

Otherwise, you'll have to build (if they're not packaged on your distro)
and install the requirements yourself. I would like to package all these
dependencies on the most common platforms (Ubuntu/Debian,
RHEL/Rocky/Fedora, Brew on macOs), but that takes some effort, and will
wait a bit. Any help is welcome.

Building and installing is simple if you have all the requirements:

``` commandline
$ cmake -S . -B build # optionally '-GNinja'
$ cmake --build build
$ sudo cmake --build build --target install
```

### Requirements

Zetk requires the following libraries:

- [SQLite3](https://sqlite.org), version `3.38.0` or newer: used for
  fulltext search of the notes, document similarity calculation and
  link/backlink searching
- [Boost](https://boost.org), version `1.76` or newer: used all over
- [oneTBB](https://github.com/oneapi-src/oneTBB), used for parallel STL
  implementation
- [libfswatch](https://github.com/emcrisostomo/fswatch), version `1.8.0`
  or newer used for database auto-update (in `zk-daemon`)
- [NamedType](https://github.com/joboccara/NamedType), for strong types,
  used to make the API more robust and also for `std::visit` dispatch in
  a couple of places

Additionally, these libraries are required for building the CLI tools:

- [args](https://github.com/Taywee/args) by Taywee, for argument parsing
  (header-only)

For development you also need [Google
test](https://github.com/google/googletest) to build and run the tests.

## Usage

### Configuration

All tools are configured with a couple of environment variables:

- `ZK_PATH`: A path to your zettelkasten folder, where you keep your
  markdown files. *(required)*
- `ZK_DB_PATH`: A path to your zettelkasten SQLite database, which
  automatically gets populated, and is used for full-text searching,
  similarity calculation, etc. If omitted, defaults to
  `$ZK_PATH/index.db`.
- `ZK_STOPWORDS`: A path to a text file, which contains whitespace-
  (preferably newline-) separated *stop words*. These words are omitted
  from the index when doing similarity comparisons between files.
  Usually, you ignore common words for your language, because their
  frequency is mostly irrelevant for document similarity (the number of
  words *the* is hardly a good similarity indicator).
  [Here](https://www.ranks.nl/stopwords)'s a collection of stop word
  sets in various languages - you may combine them into one file if you
  write in multiple languages.

### General notes on operation

All of the commandline tools require the database to exist and contain
up-to-date information on file contents.

If you use `zk-daemon`, it constantly watches for file
changes/creation/deletion in your zettelkasten folder, and updates the
database accordingly. It also creates a lockfile (`daemon.lock`) that
indicates to other tools they don't need to rescan the folder before
proceeding with their operation.

If you don't use `zk-daemon`, each time you run any of the other
utilities, it will rescan all files newer than the database (similar to
what Make does), before proceeding with its operation. You may also
trigger this rescan manually with `zk-update`, without running any other
command.

#### Zettelkasten format

The expected zettelkasten format is intended to be Markdown. It doesn't
need to be. There are only two requirements: link and tag format, the
rest of the syntax doesn't matter.

##### Links

Either denoted with double square brackets
(`[[19700101000000 The beginning of time]]`), or as regular Markdown
links `[zero epoch](19700101000000 The beginning of time)`. These links
are treated as unique prefixes to the file name, so both
`[[19700101000000 The beginning]]` and
`[[19700101000000 The beginning of time]].md` are considered to be links
to the same file, granted that there aren't other files with that
prefix, and that a file with such a prefix actually exists.

Markdown links with URLs starting with a protocol specifier
(`https://gitlab.com` or `irc://irc.libera.chat:6697`) are completely
ignored.

##### Tags

Tags can be put anywhere in the file. They start with a pound (`#`)
character, followed by lowercase letters, numbers and dashes. There must
be no whitespace between `#` and the rest of the tag. Here are some
valid tags: `#some-tag` `#tag2`. This format doesn't interfere with
Markdown headings, because those require whitespace after the `#`.

### Tools

#### `zk-fts`

    zk-fts search-query... {OPTIONS}

    Full-text search of the Zettelkasten.
    This utility has two different modes, depending on whether the --file/-f
    flag is provided or not.
    If --file/-f isnt't provided, the whole Zettelkasten is searched, and names
    of files matching the query are printed line by line.
    If a filename was provided with --file/-f, its contents are printed with the
    keywords highlighted.

    OPTIONS:

      search-query...                   query to search for in zettelkasten /
                                        single file
      -f[file], --file=[file]           search only inside this file, print
                                        results with highlighted query
      -t, --tags                        search only tags, intead of the whole
                                        file (don't prefix tagswith #)
      -0, --print0                      use null character as delimiter instead
                                        of newline
      -h, --help                        print this help and exit
      -v, --version                     output version information and exit
      "--" can be used to terminate flag options and force all following
      arguments to be treated as positional options

#### `zk-lnk`

    zk-lnk file {OPTIONS}

      Lists all backlinks to file or forward links from file.

    OPTIONS:

        file                              File in question
        The following flags and are
        exclusive:
          -b, --backlinks                   print backlinks to file (documents
                                            referencing file); default
          -l, --links                       print forward links (documents
                                            referenced in file)
        -0, --print0                      use null character as delimiter instead
                                          of newline
        -h, --help                        print this help and exit
        -v, --version                     output version information and exit
        "--" can be used to terminate flag options and force all following
        arguments to be treated as positional options

#### `zk-ls-tag`

    zk-ls-tag {OPTIONS}

      List all tags appearing in the Zettelkasten by their frequency in files.

    OPTIONS:

        -f, --freq                        Print tag's frequency before tag.
        -0, --print0                      use null character as delimiter instead
                                          of newline
        -h, --help                        print this help and exit
        -v, --version                     output version information and exit

#### `zk-tif`

    zk-tif file {OPTIONS}

      List tags appearing in a file.

    OPTIONS:

        file                              file whose tags are to be printed
        -0, --print0                      use null character as delimiter instead
                                          of newline
        -h, --help                        print this help and exit
        -v, --version                     output version information and exit
        "--" can be used to terminate flag options and force all following
        arguments to be treated as positional options

#### `zk-sim`

    zk-sim file {OPTIONS}

      Prints the argument file's similarity with other documents in the
      Zettelkasten in descending order (more similar docs first).
      Cosine similarity is used, with either TF-IDF vectorization (for text) or
      simple tag-presence testing (for tags).
      If the environment variable ZK_STOPWORDS contains a path to a text file with
      (whitespace delimited) words, these words will be used as 'stopwords': they
      will be excluded during vectorization and won't affect the similarity score
      between files.

    OPTIONS:

        file                              File to search similar files to
        -t, --tags                        calculate the similarity by tag
                                          occurence similarity (not counting)
                                          instead
        -s, --score                       Print file's similarity score before
                                          filename (0 means completely dissimilar,
                                          1 means maximally similar).
        -0, --print0                      use null character as delimiter instead
                                          of newline
        -h, --help                        print this help and exit
        -v, --version                     output version information and exit
        "--" can be used to terminate flag options and force all following
        arguments to be treated as positional options

#### `zk-rel-tags`

    zk-rel-tags tags... {OPTIONS}

      Prints all tags that occur in files which contain one of the tags given as
      arguments.

    OPTIONS:

        tags...                           Tags for which we look for related tags.
        -0, --print0                      use null character as delimiter instead
                                          of newline
        -h, --help                        print this help and exit
        -v, --version                     output version information and exit
        "--" can be used to terminate flag options and force all following
        arguments to be treated as positional options

#### `zk-tree`

    zk-tree file {OPTIONS}

      Prints a tree of files connected to a given file by links/tags.

    OPTIONS:

        file                              File used as root
        -t, --tags                        print a tree of files connected to the
                                          given file by tags instead oflinks
        -d[depth], --depth=[depth]        only print tree up to certain depth (use
                                          0 for full depth, which is the default)
        -p, --print-depth                 print depth in tree instead of printing
                                          tab spacing.
        -0, --print0                      use null character as delimiter instead
                                          of newline (only works with
                                          -p/--print-depth)
        -h, --help                        print this help and exit
        -v, --version                     output version information and exit
        "--" can be used to terminate flag options and force all following
        arguments to be treated as positional options

#### `zk-dot`

    zk-dot [filenames or tags...] {OPTIONS}

      Prints to stdout graphviz *dot* code representing the link and tag graph of
      your zettelkasten.
      By default, only direct links are shown, but you may use the -t/--tags flag
      to show tags and their links in the graph.
      If you provide the 'filenames_or_tags' args, the graph will be limited to
      files (and potentially tags) connected to those files.

    OPTIONS:

        filenames or tags...              only draw files / tags connected to
                                          these files
        -t, --tags                        also draw tags (automatically enabled if
                                          you pass a tag among
                                          `filenames_or_tags`)
        -d[depth], --depth=[depth]        draw up to certain depth, 0 means
                                          unlimited (which is the default); only
                                          makes sense when filenames or tags are
                                          provided
        -h, --help                        print this help and exit
        -v, --version                     output version information and exit
        "--" can be used to terminate flag options and force all following
        arguments to be treated as positional options

#### `zk-raw-digraph`

    zk-raw-digraph {OPTIONS}

      Outputs all links as digraph pairs (<source file>\t<dest file>)

    OPTIONS:

        -0, --print0                      use null character as delimiter instead
                                          of newline
        -h, --help                        print this help and exit
        -v, --version                     output version information and exit

#### `zk-daemon`

Running this daemon will automatically update the fulltext search index
as soon as you add/change/delete a file in your zettelkasten folder. The
daemon will create a lockfile (`daemon.lock`) in your zettelkasten
folder, indicating to other tools that it is running and that they don't
need to refresh the whole database before proceeding with their regular
operation.

    zk-daemon {OPTIONS}

      Watch for file creation / modification / deletion in $ZK_PATH and update the
      zettelkasten fulltext/link database to match the state of the $ZK_PATH folder
      in the filesystem.

    OPTIONS:

        -h, --help                        Prints help information
        -v, --version                     Prints the version number and exits

#### `zk-update`

You probably won't ever need to use this utility manually, since either
the daemon does this for you automatically, or running any of the other
tools does it automatically, but it might be useful in some script.

    zk-update {OPTIONS}

      Update the zettelkasten fulltext/link database to match the state of the
      $ZK_PATH folder in the filesystem.

    OPTIONS:

        -h, --help                        Prints help information
        -v, --version                     Prints the version number and exits

## Contributing

If you find any bugs, please report them. If the documentation is
confusing or inconsistent, report it (or fix it).

I'm open to any contributions that share the spirit of these utilities,
if the functionality is complex enough to require a custom tool (and
can't easily be cobbled together as a shell script).

I could also use some help writing the documentation, man pages and
such. Packaging and testing for other platforms is also welcome, and
I'll gladly accept changes towards that goal. I've taken care to use
only cross-platform available libs (like `libfswatch`), but I haven't
actually tested building on another platform.

You can help by solving approved issues.

## Thanks

[Simon Eskildsen](https://github.com/sirupsen) for creating the original
[zk](https://github.com/sirupsen/zk) - this project is mostly a
reimplementation of his toolset, with improvements.
