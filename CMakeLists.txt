cmake_minimum_required(VERSION 3.19)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

#
# Set up CMake local modules
#
list(
  APPEND
  CMAKE_MODULE_PATH
  "${CMAKE_SOURCE_DIR}/cmake"
  "${CMAKE_SOURCE_DIR}/cmake/modules"
)

#
# Project details
#

project(
  "zetk"
  LANGUAGES CXX
  # version inferred from git tags
)


#
# Default build type
#
set(default_build_type "Release")
if(EXISTS "${CMAKE_SOURCE_DIR}/.git")
  set(default_build_type "Debug")
endif()

if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  message(STATUS "Setting build type to '${default_build_type}' as none was specified.")
  set(
    CMAKE_BUILD_TYPE "${default_build_type}" CACHE
    STRING "Choose the type of build." FORCE
  )
  # Set the possible values of build type for cmake-gui
  set_property(
    CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS
    "Debug" "Release" "MinSizeRel" "RelWithDebInfo"
  )
endif()

#
# Import options that distinguish packaged source builds and git checkouts
#
option(${PROJECT_NAME}_SOURCE_DIST OFF)
if(EXISTS "${CMAKE_SOURCE_DIR}/cmake/SourceDist.cmake")
  include(SourceDist)
endif()

if(${PROJECT_NAME}_SOURCE_DIST)
  message(
    "Building from a source dist: header-only deps are included, version from "
    "file"
  )
else()
  message("Building from a git checkout: version obtained from git.")
endif()

#
# Set project options
#

include(StandardSettings)
include(Utils)
message(STATUS "Started CMake for ${PROJECT_NAME}...\n")

# this will allow to use same _DEBUG macro available in both Linux as well as
# Windows - MSCV environment. Easy to put Debug specific code.
if(UNIX)
  add_compile_options("$<$<CONFIG:DEBUG>:-D_DEBUG>")
endif(UNIX)

#
# Setup alternative names
#

if(${PROJECT_NAME}_USE_ALT_NAMES)
  string(TOLOWER ${PROJECT_NAME} PROJECT_NAME_LOWERCASE)
  string(TOUPPER ${PROJECT_NAME} PROJECT_NAME_UPPERCASE)
else()
  set(PROJECT_NAME_LOWERCASE ${PROJECT_NAME})
  set(PROJECT_NAME_UPPERCASE ${PROJECT_NAME})
endif()

#
# Prevent building in the source directory
#

if(PROJECT_SOURCE_DIR STREQUAL PROJECT_BINARY_DIR)
  message(
    FATAL_ERROR "In-source builds not allowed. Please make a new "
    "directory (called a build directory) and run CMake from there.\n"
  )
endif()

#
# Get CPM (CMake Package Manager)
#
include(cmake/CPM.cmake)

# Add clang-format CMake script
cpmaddpackage(
  NAME Format.cmake
  VERSION 1.7.3
  GITHUB_REPOSITORY TheLartians/Format.cmake
  OPTIONS # set to yes skip cmake formatting
  "FORMAT_SKIP_CMAKE NO"
  # path to exclude (optional, supports regular expressions)
  "CMAKE_FORMAT_EXCLUDE cmake/CPM.cmake"
)

#
# Version derivation based on git tag
#
if(NOT ${PROJECT_NAME}_SOURCE_DIST)
  include(VersionFromGit)
  version_from_git(
    INCLUDE_HASH ON
    MARK_TREE_DIRTY ON
    LOG ON
    TIMESTAMP "%Y%m%dT%H%M%SZ" UTC
  )
endif()
set(PROJECT_VERSION VERSION)

#
# Create library, setup header and source files
#

#
# Set the warnings
#
include(CompilerWarnings)

verbose_message(
  "Applied compiler warnings. "
  "Using standard ${CMAKE_CXX_STANDARD}.\n"
)

#
# Include all the targets
#

add_subdirectory(src)

message(STATUS "Added all targets.\n")
verbose_message("Successfully added all dependencies and linked against them.")
verbose_message("Install targets succesfully build. Install with `cmake --build <build_directory> --target install --config <build_config>`.")

#
# Enable Doxygen
#

include(cmake/Doxygen.cmake)


# For Windows, it is necessary to link with the MultiThreaded library.
# Depending on how the rest of the project's dependencies are linked, it might
# be necessary to change the line to statically link with the library.
#
# This is done as follows:
#
# set(CMAKE_MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Debug>:Debug>")
#
# On Linux and Mac this variable is ignored. If any issues rise from it, try
# commenting it out and letting CMake decide how to link with it.
set(CMAKE_MSVC_RUNTIME_LIBRARY "MultiThreaded$<$<CONFIG:Debug>:Debug>DLL")


message(STATUS "Finished building requirements for installing the package.\n")

#
# Unit testing setup
#

if(${PROJECT_NAME}_ENABLE_UNIT_TESTING)
  enable_testing()
  message(STATUS "Build unit tests for the project. Tests should always be found in the test folder\n")
  add_subdirectory(test)
endif()

#
# Generate a source distribution
#

include(CPackConf)

#
# Include utilities
#
include(StaticAnalyzers)
