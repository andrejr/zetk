include(CMakeFindDependencyMacro)
find_dependency(SQLite3)
find_dependency(SQLiteCpp)
find_dependency(TBB)
find_dependency(LibFsWatch)
find_dependency(NamedType)
find_dependency(Boost COMPONENTS filesystem iostreams log)

include(${CMAKE_CURRENT_LIST_DIR}/zetkTargets.cmake)
