set(
  sources
  src/tmp.cpp
  src/db.cpp
  src/parser.cpp
  src/cos_similarity.cpp
  src/fs.cpp
  src/read.cpp
  src/env.cpp
)

set(
  exe_sources
  src/main.cpp
)

set(
  headers
  include/zetk/cos_similarity.hpp
  include/zetk/db.hpp
  include/zetk/env.hpp
  include/zetk/fs.hpp
  include/zetk/home.hpp
  include/zetk/init.hpp
  include/zetk/parser.hpp
  include/zetk/read.hpp
  include/zetk/tmp.hpp
)
