# - Try to find LibFswatch
# Once done this will define
#  LibFsWatch_FOUND - System has LibFswatch
#  LibFsWatch_INCLUDE_DIRS - The LibFswatch include directories
#  LibFsWatch_LIBRARIES - The libraries needed to use LibFswatch
#  LibFsWatch_DEFINITIONS - Compiler switches required for using LibFswatch

find_package(PkgConfig)
pkg_check_modules(PC_LIBFSWATCH QUIET libfswatch)
set(LibFsWatch_DEFINITIONS ${PC_LIBFSWATCH_CFLAGS_OTHER})
mark_as_advanced(LibFsWatch_DEFINITIONS)

# Look for necessary header
find_path(
  LibFsWatch_INCLUDE_DIR libfswatch/c++/event.hpp
  HINTS ${PC_LIBFSWATCH_INCLUDEDIR} ${PC_LIBFSWATCH_INCLUDE_DIRS}
  PATH_SUFFIXES libfswatch
)
mark_as_advanced(LibFsWatch_INCLUDE_DIR)

# Look for necessary library
find_library(
  LibFsWatch_LIBRARY NAMES fswatch libfswatch
  HINTS ${PC_LIBFSWATCH_LIBDIR} ${PC_LIBFSWATCH_LIBRARY_DIRS}
)
mark_as_advanced(LibFsWatch_LIBRARY)

# Extract version info from header
if(LibFsWatch_INCLUDE_DIR)
  file(
    STRINGS ${LibFsWatch_INCLUDE_DIR}/libfswatch/c++/event.hpp _ver_line
    REGEX "^[ \t\r\n]*\\*[ \t\r\n]*@version[ \t\r\n]*[0-9]+\\.[0-9]+\\.[0-9]+"
    LIMIT_COUNT 1
  )
  string(
    REGEX MATCH "[0-9]+\\.[0-9]+\\.[0-9]+"
    LibFsWatch_VERSION "${_ver_line}"
  )
  unset(_ver_line)
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
  LibFsWatch
  REQUIRED_VARS LibFsWatch_LIBRARY LibFsWatch_INCLUDE_DIR
  VERSION_VAR LibFsWatch_VERSION
)

if(LibFsWatch_FOUND)
  set(LibFsWatch_LIBRARIES ${LibFsWatch_LIBRARY})
  set(LibFsWatch_INCLUDE_DIRS ${LibFsWatch_INCLUDE_DIR})

  if(NOT TARGET LibFsWatch::LibFsWatch)
    add_library(LibFsWatch::LibFsWatch UNKNOWN IMPORTED)
    set_target_properties(
      LibFsWatch::LibFsWatch PROPERTIES
      IMPORTED_LOCATION "${LibFsWatch_LIBRARY}"
      INTERFACE_INCLUDE_DIRECTORIES "${LibFsWatch_INCLUDE_DIR}"
    )
  endif()
endif()
