# - Try to find LibFswatch
# Once done this will define
#  TayweeArgs_FOUND - System has LibFswatch
#  TayweeArgs_INCLUDE_DIRS - The LibFswatch include directories
#  TayweeArgs_LIBRARIES - The libraries needed to use LibFswatch
#  TayweeArgs_DEFINITIONS - Compiler switches required for using LibFswatch

find_package(PkgConfig)
pkg_check_modules(PC_TAYWEEARGS QUIET libfswatch)

# Look for necessary header
find_path(TayweeArgs_INCLUDE_DIR args.hxx)
mark_as_advanced(TayweeArgs_INCLUDE_DIR)

# Extract version info from header
if(TayweeArgs_INCLUDE_DIR)
  file(
    STRINGS ${TayweeArgs_INCLUDE_DIR}/args.hxx _ver_line
    REGEX "^#define ARGS_VERSION \"[0-9]+\\.[0-9]+\\.[0-9]+\"$"
    LIMIT_COUNT 1
  )
  string(
    REGEX MATCH "[0-9]+\\.[0-9]+\\.[0-9]+"
    TayweeArgs_VERSION "${_ver_line}"
  )
  unset(_ver_line)
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
  TayweeArgs
  REQUIRED_VARS TayweeArgs_INCLUDE_DIR
  VERSION_VAR TayweeArgs_VERSION
)

if(TayweeArgs_FOUND)
  set(TayweeArgs_INCLUDE_DIRS ${TayweeArgs_INCLUDE_DIR})

  if(NOT TARGET TayweeArgs::TayweeArgs)
    add_library(TayweeArgs::TayweeArgs INTERFACE IMPORTED)
    set_target_properties(
      TayweeArgs::TayweeArgs PROPERTIES
      INTERFACE_INCLUDE_DIRECTORIES "${TayweeArgs_INCLUDE_DIR}"
    )
  endif()
endif()
