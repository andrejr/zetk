// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#include "zetk/parser.hpp"

#include <algorithm>
#include <iterator>
#include <regex>

#include <boost/algorithm/string.hpp>

namespace libzetk {

std::string extract_tags(const std::string &text)
{
    static const std::regex tag_rx { R"(#[-A-Za-z0-9]+)" };
    std::sregex_token_iterator iter(text.cbegin(), text.cend(), tag_rx);
    std::sregex_token_iterator end {};

    std::ostringstream ss;
    std::copy(iter, end, std::ostream_iterator<std::string>(ss, " "));
    return ss.str();
}

LinkAndCountMap extract_links(const std::string &text)
{
    static const std::regex tag_rx {
        R"((?:(?:^|[^!])\[(?:[\s\S]*?)\]\(([\s\S]+?)\))|(?:\[\[([\s\S]+?)\]\]))"
    };
    std::sregex_iterator iter(text.cbegin(), text.cend(), tag_rx);
    std::sregex_iterator end {};

    LinkAndCountMap result;

    for (auto &e = iter; iter != end; ++iter) {
        auto group = (*e)[1].matched ? (*e)[1] : (*e)[2];
        std::string key = boost::trim_copy(static_cast<std::string>(group));

        static const std::regex url_with_protocol { R"(^[a-zA-Z0-9]+://)" };
        if (!std::regex_search(key, url_with_protocol)) {
            result[key] += 1;
        }
    }

    return result;
}

} // namespace libzetk
