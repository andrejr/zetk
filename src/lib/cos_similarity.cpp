// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#include "zetk/cos_similarity.hpp"

#include <cmath>

#include <algorithm>
#include <execution>
#include <iostream>
#include <mutex>
#include <numeric>

namespace libzetk {

double cos_similarity_between_vectors(const std::vector<double> &a,
                                      const std::vector<double> &b)
{
    auto dot_product = std::transform_reduce(
        std::execution::par_unseq, a.cbegin(), a.cend(), b.cbegin(), 0.0);
    auto a_squared_product = std::transform_reduce(
        std::execution::par_unseq, a.cbegin(), a.cend(), a.cbegin(), 0.0);
    auto b_squared_product = std::transform_reduce(
        std::execution::par_unseq, b.cbegin(), b.cend(), b.cbegin(), 0.0);
    auto denominator =
        std::sqrt(a_squared_product) * std::sqrt(b_squared_product);
    if (denominator == 0.0) {
        return 0.0;
    }
    return dot_product / (denominator);
}

SimilaritySet
find_files_similar_to_file(const std::string &file,
                           std::unordered_map<std::string, std::vector<double>>
                               &word_measures_per_file)
{
    auto main_file_word_scores = std::move(word_measures_per_file[file]);
    word_measures_per_file.erase(file);

    SimilaritySet results;
    std::mutex results_mutex;
    std::for_each(
        std::execution::par_unseq, word_measures_per_file.cbegin(),
        word_measures_per_file.cend(),
        [&main_file_word_scores, &results, &results_mutex](const auto &p) {
            const auto &[curr_file, word_scores] = p;
            std::lock_guard lock(results_mutex);
            results.emplace(cos_similarity_between_vectors(
                                main_file_word_scores, word_scores),
                            curr_file);
        });
    return results;
}

} // namespace libzetk
