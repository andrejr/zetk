// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#include "zetk/fs.hpp"

#include <algorithm>
#include <chrono>
#include <fstream>
#include <vector>

namespace libzetk {
using std::filesystem::path;

std::string read_file(const std::filesystem::path &file_path)
{
    std::ifstream ifs(file_path,
                      std::ios::in | std::ios::binary | std::ios::ate);

    auto file_size = ifs.tellg();

    std::vector<char> bytes;
    bytes.reserve(static_cast<std::string::size_type>(file_size));

    ifs.seekg(0, std::ios::beg);
    ifs.read(bytes.data(), file_size);

    return { bytes.data(), static_cast<std::string::size_type>(file_size) };
}

time_t _file_time_type_to_time_t(std::filesystem::file_time_type file_time)
{
    auto duration = file_time.time_since_epoch();
    using std::chrono::seconds;
    return std::chrono::duration_cast<seconds>(duration).count();
}

time_t get_file_mtime(const std::filesystem::path &file_path)
{
    auto file_time = std::filesystem::last_write_time(file_path);
    return _file_time_type_to_time_t(file_time);
}

path resolve_potential_symlink(path potential_symlink)
{
    while (std::filesystem::is_symlink(potential_symlink)) {
        potential_symlink = std::filesystem::read_symlink(potential_symlink);
    }
    return potential_symlink;
}

std::filesystem::path boost_path_to_std_path(const boost::filesystem::path &p)
{
    return p.string();
}

} // namespace libzetk
