// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#include "zetk/read.hpp"

#include "zetk/db.hpp"
#include "zetk/fs.hpp"
#include "zetk/parser.hpp"

#include <algorithm>
#include <execution>
#include <thread>

#include <boost/log/attributes/named_scope.hpp>
#include <boost/log/trivial.hpp>

namespace libzetk {

void read_file_into_db(const path &file_path, SQLite::Database &db,
                       bool delete_prev_entry)
{
    BOOST_LOG_NAMED_SCOPE("db_sync");
    auto body = libzetk::read_file(file_path);

    auto tags = extract_tags(body);
    auto links = extract_links(body);

    auto title = file_path.filename();

    if (delete_prev_entry) {
        delete_file_from_db(filename_t(title), db);
        BOOST_LOG_TRIVIAL(info)
            << "Updated existing DB entry for " << title << ".";
    } else {
        BOOST_LOG_TRIVIAL(info) << "Created a DB entry for " << title << ".";
    }
    add_file_to_db(filename_t(title), body, tag_t(std::move(tags)),
                   get_file_mtime(file_path), db);
    add_or_update_links_for_file(filename_t(title), links, db);
}

void synchronize_folder_and_db(const path &folder_path, SQLite::Database &db)
{
    BOOST_LOG_NAMED_SCOPE("db_sync");
    SQLite::Transaction transaction(db);

    auto files_in_db_map = get_files_in_db_map(db);

    // iterate through markdown files in folder
    std::for_each(std::execution::par_unseq,
                  std::filesystem::directory_iterator { folder_path },
                  std::filesystem::directory_iterator {},
                  [&files_in_db_map, &db](const auto &dir_entry) {
                      // only handle files that are .md after symlink
                      // resolution
                      if (auto resolved_path =
                              resolve_potential_symlink(dir_entry.path());
                          std::filesystem::is_regular_file(resolved_path)
                          && resolved_path.extension() == ".md") {

                          auto title = resolved_path.filename();

                          // if file is in the db already, check the mtime
                          if (files_in_db_map.contains(title)) {
                              auto db_mtime = files_in_db_map[title];
                              auto file_mtime = get_file_mtime(resolved_path);
                              if (file_mtime > db_mtime) {
                                  read_file_into_db(resolved_path, db, true);
                              }
                              // If a file is in the db, erase it from the
                              // files_in_db map. After iterating through all
                              // files, the only entries in files_in_db are for
                              // files that don't exist in the filesystem
                              // anymore.
                              files_in_db_map.erase(title);
                          } else {
                              read_file_into_db(resolved_path, db, false);
                          }
                      }
                  });

    // delete the remaining, non-visited file entries from the database
    for (auto const &remaining_db_entry : files_in_db_map) {
        const auto &title = remaining_db_entry.first;
        BOOST_LOG_TRIVIAL(info) << "Removed the DB entry for" << title
                                << " because the file no longer exists.";
        delete_file_from_db(filename_t(title), db);
    }

    transaction.commit();
}

} // namespace libzetk
