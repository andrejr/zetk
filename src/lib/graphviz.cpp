// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#include "zetk/graphviz.hpp"

#include "zetk/db.hpp"
#include "zetk/overload.hpp"

#include <algorithm>
#include <ostream>
#include <queue>
#include <ranges>
#include <regex>

#include <boost/algorithm/string/replace.hpp>

namespace libzetk {

template <typename Key, typename T, typename Hash, typename KeyEqual,
          typename Allocator>
bool pair_in_ummap(
    const Key key, const T val,
    std::unordered_multimap<Key, T, Hash, KeyEqual, Allocator> mmap)
{
    auto [first, last] = mmap.equal_range(key);
    auto r = std::ranges::subrange(first, last);
    return std::ranges::any_of(
        r, [&val](auto key_val_pair) { return key_val_pair.second == val; });
}

static inline std::string dash_to_underscore(const std::string &tag)
{
    return boost::replace_all_copy(tag, "-"s, "_"s);
}

std::string transform_filename(const std::string &filename)
{
    static const std::regex date_title_rx { R"((\d{12}) (.*?)\.md)" };
    static const std::regex plain_title_rx { R"((.*?)\.md)" };

    if (std::smatch match; std::regex_match(filename, match, date_title_rx)) {
        auto time = match[1].str();
        auto title = match[2].str();
        return time + "\\n" + title;
    }
    if (std::smatch match; std::regex_match(filename, match, plain_title_rx)) {
        return match[1].str();
    }
    return filename;
}

FileIdsAndLinks get_fileids_and_links_connected_to_file(const FileIds &fileids,
                                                        SQLite::Database &db,
                                                        size_t depth)
{
    struct entry_t {
        rowid_t fileid;
        size_t depth;
    };
    std::stack<entry_t> s;

    for (const auto &fileid : fileids) {
        s.push({ fileid, 0U });
    }

    auto all_links = get_links_as_id_ummap(db);

    std::unordered_set<rowid_t> files;
    using link_t = std::pair<rowid_t, rowid_t>;
    std::unordered_set<link_t, boost::hash<link_t>> res_links;

    while (!s.empty()) {
        auto e = s.top();
        s.pop();

        // append result
        if (!files.contains(e.fileid)) {
            files.emplace(e.fileid);

            // push next
            if (depth == 0 || e.depth < depth) {
                auto [begin, end] = all_links.equal_range(e.fileid);
                auto linked_files = std::ranges::subrange(begin, end);
                for (auto &[_, f] : linked_files) {
                    s.push({ .fileid = f, .depth = e.depth + 1 });
                    res_links.emplace(e.fileid, f);
                }
            }
        }
    }

    return { std::move(files), std::move(res_links) };
}

TagsAndTagFileLinks
get_tags_and_links_to_tags_from_files(const FileIdsOrTags &fileids_or_tags,
                                      SQLite::Database &db, size_t depth)
{
    using fileid_or_tag_t = std::variant<rowid_t, tag_t>;

    struct entry_t {
        fileid_or_tag_t file_or_tag;
        db_cnt_t level;
    };

    depth *= 2;

    auto tag_file_map = get_tag_fileid_ummap(db);
    auto file_tag_map = get_fileid_tag_ummap(db);

    std::unordered_set<std::string> tags;
    std::unordered_set<rowid_t> res_fileids;
    TagFileLinks res_links;

    std::queue<entry_t> q;
    for (const auto &fileid_or_tag : fileids_or_tags) {
        q.push({ fileid_or_tag, 0 });
    }

    while (!q.empty()) {
        auto [file_or_tag, level] = std::move(q.front());
        q.pop();

        std::visit(
            overload { [&file_tag_map, depth, level = level, &tags, &q,
                        &res_fileids](const rowid_t &f) {
                          res_fileids.insert(f);
                          if (depth == 0 || level < depth) {
                              auto [begin, end] = file_tag_map.equal_range(f);
                              auto curr_tags =
                                  std::ranges::subrange(begin, end);
                              for (auto &[_, tag] : curr_tags) {
                                  if (!tags.contains(tag)) {
                                      tags.insert(tag);
                                      q.push({ tag_t(tag), level + 1 });
                                  }
                              }
                          }
                      },
                       [&tag_file_map, &q, level = level, &res_links,
                        &res_fileids, depth](const tag_t &tag) {
                           auto [begin, end] =
                               tag_file_map.equal_range(tag.get());
                           auto file_ids = std::ranges::subrange(begin, end);
                           for (auto &[_, f] : file_ids) {
                               if (!res_fileids.contains(f)) {
                                   q.push({ f, level + 1 });
                               }
                               if (!pair_in_ummap(tag.get(), f, res_links)
                                   && (depth == 0 || level < depth)) {
                                   res_links.emplace(tag.get(), f);
                               }
                           }
                       } },
            file_or_tag);
    }
    return { .tags = std::move(tags),
             .fileids = std::move(res_fileids),
             .tag_links = std::move(res_links) };
}

void output_dot(bool draw_tags, std::ostream &os, SQLite::Database &db)
{
    auto filename_id_pairs = list_filenames_and_db_ids(db);
    auto links = get_links_as_id_pairs(db);

    os << "digraph {\n\n"s;

    os << "  subgraph Files {\n"s
       << "    node [ shape = rectangle ]\n\n"s;

    for (const auto &[filename, id] : filename_id_pairs) {
        os << "    "s << id << " [ label = \""s << transform_filename(filename)
           << "\" ]\n";
    }

    os << "  }\n\n"s
       << "  subgraph Links {\n"s;

    for (const auto &[s, d] : links) {
        os << "    "s << s << " -> "s << d << "\n";
    }

    os << "  }\n"s;

    if (draw_tags) {
        os << "\n"s
           << "  subgraph Tags {\n\n"s
           << "    node [ shape = oval ]\n"s
           << "    edge [ dir = none ]\n\n"s;

        auto tags = list_all_tags(db);
        for (const auto &tag : tags) {
            os << "    "s << dash_to_underscore(tag) << " [ label = \"#"s
               << tag << "\" ]\n";
        }

        os << "\n"s;

        auto tag_fileid_pairs = get_tag_fileid_pairs(db);
        for (const auto &[tag, fileid] : tag_fileid_pairs) {
            os << "    "s << dash_to_underscore(tag) << " -> "s << fileid
               << "\n";
        }

        os << "  }\n"s;
    }

    os << "}\n"s;
}
template <typename T>
void print_container(std::ostream &os, const T &container,
                     const std::string &delimiter = ", "s)
{
    std::copy(
        std::begin(container), std::end(container),
        std::ostream_iterator<typename T::value_type>(os, delimiter.c_str()));
}

void output_dot_subgraph(const FileIdsOrTags &fileids_or_tags, bool draw_tags,
                         std::ostream &os, SQLite::Database &db, size_t depth)
{

    FileIds just_files;
    bool tag_encountered = false;
    std::ranges::for_each(
        fileids_or_tags, [&just_files, &tag_encountered](const auto &e) {
            return std::visit(overload { [&just_files](const rowid_t &fileid) {
                                            just_files.insert(fileid);
                                        },
                                         [&tag_encountered](const tag_t &) {
                                             tag_encountered = true;
                                         } },
                              e);
        });

    auto [linked_file_ids, links] =
        get_fileids_and_links_connected_to_file(just_files, db, depth);

    auto [tags, neighbor_file_ids, tag_links] = (draw_tags || tag_encountered)
        ? get_tags_and_links_to_tags_from_files(fileids_or_tags, db, depth)
        : TagsAndTagFileLinks {};

    auto filename_id_pairs = list_filenames_and_db_ids(db);

    os << "digraph {\n\n"s;

    os << "  subgraph Files {\n"s
       << "    node [ shape = rectangle ]\n\n"s;

    for (const auto &[filename, id] : filename_id_pairs) {
        if (linked_file_ids.contains(id) || neighbor_file_ids.contains(id)) {
            os << "    "s << id << " [ label = \""s
               << transform_filename(filename) << "\" ]\n";
        }
    }

    os << "  }\n\n"s
       << "  subgraph Links {\n"s;

    for (const auto &[s, d] : links) {
        os << "    "s << s << " -> "s << d << "\n";
    }

    os << "  }\n"s;

    if (draw_tags) {
        os << "\n"s
           << "  subgraph Tags {\n\n"s
           << "    node [ shape = oval ]\n"s
           << "    edge [ dir = none ]\n\n"s;

        for (const auto &tag : tags) {
            os << "    "s << dash_to_underscore(tag) << " [ label = \"#"s
               << tag << "\" ]\n";
        }

        os << "\n"s;

        for (const auto &[tag, fileid] : tag_links) {
            os << "    "s << dash_to_underscore(tag) << " -> "s << fileid
               << "\n";
        }

        os << "  }\n"s;
    }

    os << "}\n"s;
}

}
