// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#include "zetk/env.hpp"

#include "zetk/fs.hpp"

namespace libzetk {

std::optional<std::string> get_env_var(const std::string_view &key) noexcept
{
    const auto *ret = std::getenv(key.data());
    if (ret != nullptr) {
        return ret;
    }
    return std::nullopt;
}

} // namespace libzetk
