// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#include "zetk/db.hpp"

#include "zetk/cos_similarity.hpp"
#include "zetk/int_casts.hpp"
#include "zetk/overload.hpp"

#include <cmath>

#include <filesystem>
#include <queue>
#include <ranges>
#include <regex>
#include <stack>
#include <variant>

#include <boost/log/trivial.hpp>
#include <type_traits>

using SQLite::Statement;
using std::filesystem::path;
using namespace std::string_literals;
using SQLite::Database;

namespace libzetk {
// When it comes to 8-byte datatypes, SQLite3 only supports int64_t
// This means that the values mut be obtained from the DB as signed, if rowid_t
// or db_cnt_t are 8-byte values (on systems where size_t is uint64_t, like
// x64, ARM64) and then cast to size_t.
// On 32bit systems where size_t is uint32_t, we may obtain it directly from
// SQLite3.
using rowid_db_get_t =
    std::conditional_t<(sizeof(rowid_t) == 8), std::make_signed_t<rowid_t>,
                       rowid_t>;
using db_cnt_get_t = std::conditional_t<(sizeof(db_cnt_t) == 8),
                                        std::make_signed_t<rowid_t>, rowid_t>;

constexpr int sqlite_busy_timeout = 200;

Database create_db(const path &db_path)
{
    Database db { db_path, SQLite::OPEN_READWRITE | SQLite::OPEN_CREATE,
                  sqlite_busy_timeout };

    db.exec("CREATE VIRTUAL TABLE IF NOT EXISTS zk\n"
            "USING fts5(\n"
            "   title, \n"
            "   body, \n"
            "   tags, \n"
            "   mtime UNINDEXED, \n"
            "   prefix = 3, \n"
            "   tokenize = \"unicode61 tokenchars '-'\",\n"
            "   detail = \"full\"\n"
            ");");

    db.exec("INSERT INTO zk (zk, rank) \n"
            "VALUES('rank', 'bm25(2.0, 1.0, 5.0, 0.0)');");

    db.exec("CREATE TABLE IF NOT EXISTS links(\n"
            "   link_id INTEGER PRIMARY KEY,\n"
            "   origin_id INTEGER NOT NULL,\n"
            "   dest_id INTEGER NOT NULL,\n"
            "   number_of_links NOT NULL,\n"
            "   FOREIGN KEY(origin_id)\n"
            "       REFERENCES zk(rowid)\n"
            "           ON DELETE CASCADE,\n"
            "   FOREIGN KEY(dest_id)\n"
            "       REFERENCES zk(rowid)\n"
            "           ON DELETE CASCADE,\n"
            "   UNIQUE (origin_id, dest_id)\n"
            ");");

    db.exec("CREATE VIRTUAL TABLE IF NOT EXISTS zk_vocab_col\n"
            "   USING fts5vocab(zk, col);");

    db.exec("CREATE VIRTUAL TABLE IF NOT EXISTS zk_vocab_row\n"
            "   USING fts5vocab(zk, row);");

    db.exec("CREATE VIRTUAL TABLE IF NOT EXISTS zk_vocab_ins\n"
            "   USING fts5vocab(zk, instance);");

    return db;
}

Database open_db(const path &db_path)
{
    Database db { db_path, SQLite::OPEN_READWRITE | SQLite::OPEN_CREATE,
                  sqlite_busy_timeout };
    return db;
}

void add_file_to_db(const filename_t &title, const std::string &body,
                    const tag_t &tags, mtime_t mtime, Database &db)
{
    Statement query(db,
                    "INSERT INTO zk (title, body, tags, mtime)\n"
                    "   VALUES (?, ?, ?, ?);");
    query.bind(1, title.get());
    query.bind(2, body);
    query.bind(3, tags.get());
    query.bind(4, mtime);
    query.exec();
}

std::vector<std::pair<std::string, mtime_t>> list_files_in_db(Database &db)
{
    Statement query(db, "SELECT title, mtime FROM zk ORDER BY title ASC;");
    std::vector<std::pair<std::string, mtime_t>> return_val;
    while (query.executeStep()) {
        std::string title = query.getColumn(0);
        const mtime_t mtime = query.getColumn(1);
        return_val.emplace_back(std::move(title), mtime);
    }

    return return_val;
}

std::vector<std::pair<std::string, rowid_t>>
list_filenames_and_db_ids(Database &db)
{
    Statement query(db, "SELECT title, rowid FROM zk ORDER BY title ASC;");
    std::vector<std::pair<std::string, rowid_t>> res;
    while (query.executeStep()) {
        std::string title = query.getColumn(0);
        rowid_db_get_t rowid = query.getColumn(1);
        res.emplace_back(std::move(title), static_cast<rowid_t>(rowid));
    }

    return res;
}

std::unordered_map<std::string, mtime_t> get_files_in_db_map(Database &db)
{
    Statement query(db, "SELECT title, mtime FROM zk ORDER BY title ASC;");
    std::unordered_map<std::string, mtime_t> results;
    while (query.executeStep()) {
        std::string title = query.getColumn(0);
        const mtime_t mtime = query.getColumn(1);
        results[std::move(title)] = mtime;
    }

    return results;
}

void delete_file_from_db(const filename_t &title, Database &db)
{
    Statement query(db, "DELETE FROM zk WHERE title = ?;");
    query.bind(1, title.get());
    query.exec();
}

std::optional<std::string> get_file_contents_from_db(const filename_t &file,
                                                     Database &db)
{
    Statement query(db, "SELECT body FROM zk WHERE title = ?;");
    query.bind(1, file.get());

    if (query.executeStep()) {
        return { query.getColumn(0) };
    }
    return {};
}

std::optional<FileMetadata> get_file_metadata(filename_t title, Database &db)
{
    Statement query(db, "SELECT mtime, rowid FROM zk WHERE title = ?;");
    query.bind(1, title.get());
    if (query.executeStep()) {
        mtime_t mtime = query.getColumn(0);
        rowid_db_get_t rowid = query.getColumn(1);
        return { {
            .title = std::move(title.get()),
            .id = static_cast<rowid_t>(rowid),
            .mtime = mtime,
        } };
    }
    return {};
}

rowid_t _get_file_id_from_filename(const filename_t &filename, Database &db)
{
    Statement query(db, "SELECT rowid from zk WHERE title = ?");
    query.bind(1, filename.get());

    if (query.executeStep()) {
        rowid_db_get_t origin_id = query.getColumn(0);
        return static_cast<rowid_t>(origin_id);
    }
    throw not_found_error("Couldn't find entry for file `" + filename.get()
                          + "` in the database.");
}

std::optional<rowid_t>
get_unique_file_id_from_prefix(const filename_t &filename, Database &db)
{
    static const std::regex percent("%");
    static const std::regex underscore("_");

    auto filename_pfx_expr =
        std::regex_replace(filename.get(), percent, "\\%"s);
    std::regex_replace(filename_pfx_expr, underscore, "\\_"s);
    filename_pfx_expr += "%"s;

    Statement query(db, "SELECT rowid from zk WHERE title LIKE ? ESCAPE '\\'");
    query.bind(1, filename_pfx_expr);

    if (query.executeStep()) {
        rowid_db_get_t rowid = query.getColumn(0);
        if (query.executeStep()) {
            // more than one result
            return {};
        }
        return rowid;
    }
    return {};
}

std::optional<std::string>
get_unique_filename_from_prefix(const filename_t &filename, Database &db)
{
    static const std::regex percent("%");
    static const std::regex underscore("_");

    auto filename_pfx_expr =
        std::regex_replace(filename.get(), percent, "\\%"s);
    filename_pfx_expr =
        std::regex_replace(filename_pfx_expr, underscore, "\\_"s);
    filename_pfx_expr += "%"s;

    Statement query(db, "SELECT title from zk WHERE title LIKE ? ESCAPE '\\'");
    query.bind(1, filename_pfx_expr);

    if (query.executeStep()) {
        std::string title = query.getColumn(0);
        if (query.executeStep()) {
            // more than one result
            return {};
        }
        return title;
    }
    return {};
}

void add_or_update_links_for_file(const filename_t &title,
                                  const LinkAndCountMap &links, Database &db)
{
    Statement del_query(db,
                        "DELETE FROM links\n"
                        "   WHERE links.origin_id = (SELECT rowid from zk "
                        "WHERE title = ?);\n");
    del_query.bind(1, title.get());
    del_query.exec();

    rowid_t origin_id = _get_file_id_from_filename(title, db);

    for (const auto &link : links) {
        const auto &dest_name = link.first;
        const auto &number_of_links = link.second;

        if (auto dest_rowid =
                get_unique_file_id_from_prefix(filename_t(dest_name), db)) {

            Statement link_insert_query(
                db,
                "INSERT OR IGNORE INTO links(origin_id, dest_id, \n"
                "    number_of_links)\n"
                "VALUES (\n"
                "    ?,\n"
                "    ?,\n"
                "    ? \n"
                ");");
            link_insert_query.bind(1, static_cast<rowid_db_get_t>(origin_id));
            link_insert_query.bind(2,
                                   static_cast<rowid_db_get_t>(*dest_rowid));
            link_insert_query.bind(3,
                                   static_cast<db_cnt_get_t>(number_of_links));
            link_insert_query.exec();
        } else {
            BOOST_LOG_TRIVIAL(warning)
                << "Link `" << dest_name
                << "` doesn't point to an existing file "
                   "or is ambiguous (is a prefix to more than one filename)";
        }
    }
}

std::vector<std::pair<std::string, db_cnt_t>>
list_files_linking_to_file(const filename_t &destination_file, Database &db)
{
    rowid_t destination_id = _get_file_id_from_filename(destination_file, db);
    Statement query(db,
                    "SELECT zk.title as title, links.number_of_links\n"
                    "FROM links JOIN zk ON links.origin_id = zk.rowid\n"
                    "WHERE links.dest_id = ?\n"
                    "ORDER BY title ASC;");
    query.bind(1, static_cast<rowid_db_get_t>(destination_id));

    std::vector<std::pair<std::string, db_cnt_t>> return_val;
    while (query.executeStep()) {
        std::string title = query.getColumn(0);
        const db_cnt_get_t number_of_links = query.getColumn(1);
        return_val.emplace_back(std::move(title),
                                static_cast<db_cnt_t>(number_of_links));
    }
    return return_val;
}

std::vector<std::pair<std::string, db_cnt_t>>
list_links_in_file(const filename_t &origin_file, Database &db)
{
    auto origin_id = _get_file_id_from_filename(origin_file, db);
    Statement query(db,
                    "SELECT zk.title as title, links.number_of_links\n"
                    "FROM links JOIN zk ON links.dest_id = zk.rowid\n"
                    "WHERE links.origin_id = ?\n"
                    "ORDER BY title ASC;");
    query.bind(1, static_cast<rowid_db_get_t>(origin_id));

    std::vector<std::pair<std::string, db_cnt_t>> return_val;
    while (query.executeStep()) {
        std::string title = query.getColumn(0);
        const db_cnt_get_t number_of_links = query.getColumn(1);
        return_val.emplace_back(std::move(title),
                                static_cast<db_cnt_t>(number_of_links));
    }
    return return_val;
}

std::vector<std::pair<std::string, db_cnt_t>> list_tags_by_freq(Database &db)
{
    Statement query(db,
                    "SELECT term, doc\n"
                    "FROM zk_vocab_col\n"
                    "WHERE col = 'tags'\n"
                    "ORDER BY doc DESC\n"
                    ";");

    std::vector<std::pair<std::string, db_cnt_t>> results;
    while (query.executeStep()) {
        std::string title = query.getColumn(0);
        const db_cnt_get_t count = query.getColumn(1);
        results.emplace_back(std::move(title), count);
    }
    return results;
}

std::vector<FtsResult>
fulltext_search_all_files(const std::string &search, Database &db,
                          const std::string_view &highlight_start,
                          const std::string_view &highlight_stop)
{

    Statement query(db,
                    "SELECT title, rank, highlight(zk, 1, ?, ?), tags\n"
                    "FROM zk\n"
                    "WHERE zk MATCH ?\n"
                    "ORDER BY rank\n"
                    ";");
    query.bind(1, highlight_start.data());
    query.bind(2, highlight_stop.data());
    query.bind(3, search);

    std::vector<FtsResult> results;

    while (query.executeStep()) {
        std::string title = query.getColumn(0);
        const rank_t rank = query.getColumn(1);
        std::string body = query.getColumn(2);
        std::string tags = query.getColumn(3);
        results.emplace_back(std::move(title), std::move(body),
                             std::move(tags), rank);
    }

    return results;
}

std::optional<FtsResult>
fulltext_search_in_file(const std::string &search, const filename_t &file,
                        Database &db, const std::string_view &highlight_start,
                        const std::string_view &highlight_stop)
{
    Statement query(db,
                    "SELECT title, rank, highlight(zk, 1, ?, ?), tags\n"
                    "FROM zk\n"
                    "WHERE title = ? AND zk MATCH ?\n"
                    "ORDER BY rank\n"
                    ";");
    query.bind(1, highlight_start.data());
    query.bind(2, highlight_stop.data());
    query.bind(3, file.get());
    query.bind(4, search);

    if (query.executeStep()) {
        const std::string title = query.getColumn(0);
        const rank_t rank = query.getColumn(1);
        const std::string body = query.getColumn(2);
        const std::string tags = query.getColumn(3);
        return { { title, body, tags, rank } };
    }

    return {};
}

std::string wrap_fts_query(const std::string &query)
{
    static const std::regex colon(":");
    static const std::regex dash("-");

    auto retval = std::regex_replace(query, colon, "\":\""s);
    retval = std::regex_replace(retval, dash, "\"-\""s);
    return retval;
}

std::vector<std::string>
get_vocab_words(Database &db,
                const std::unordered_set<std::string> &stop_words,
                const std::string &column)
{
    Statement query(db,
                    "SELECT DISTINCT term\n"
                    "FROM zk_vocab_col\n"
                    "WHERE col = ?\n"
                    "ORDER BY term ASC\n"
                    ";");
    query.bind(1, column);
    std::vector<std::string> results;

    while (query.executeStep()) {
        std::string term = query.getColumn(0);
        if (!stop_words.contains(term)) {
            results.emplace_back(std::move(term));
        }
    }

    return results;
}

std::unordered_map<std::string, std::vector<rank_t>>
get_word_frequencies_per_file(
    Database &db, const std::unordered_set<std::string> &stop_words)
{
    Statement query(
        db,
        "SELECT title as filename, term, COUNT(*) as count \n"
        "FROM zk_vocab_ins JOIN zk ON zk_vocab_ins.doc = zk.rowid\n"
        "WHERE col = 'body'\n"
        "GROUP BY filename, term \n"
        ";");

    auto vocab = get_vocab_words(db, stop_words);
    auto vocab_len = vocab.size();
    std::unordered_map<std::string, rowid_t> term_to_index_map;
    for (rowid_t i = 0; i < vocab_len; ++i) {
        term_to_index_map[vocab[i]] = i;
    }

    std::unordered_map<std::string, std::vector<rank_t>> results;
    while (query.executeStep()) {
        const std::string filename = query.getColumn(0);
        const std::string term = query.getColumn(1);
        const rank_t count = query.getColumn(2);

        if (term_to_index_map.contains(term)) {
            if (!results.contains(filename)) {
                results[filename] = std::vector<rank_t>(vocab_len);
            }
            auto word_idx = term_to_index_map[term];
            results[filename][word_idx] = count;
        }
    }

    return results;
}

std::unordered_map<std::string, std::vector<rank_t>>
get_tag_incidence_per_file(Database &db,
                           const std::unordered_set<std::string> &ignored_tags)
{
    Statement query(
        db,
        "SELECT title as filename, term, 1 as count \n"
        "FROM zk_vocab_ins JOIN zk ON zk_vocab_ins.doc = zk.rowid\n"
        "WHERE col = 'tags'\n"
        "GROUP BY filename, term \n"
        ";");

    auto vocab = get_vocab_words(db, ignored_tags, "tags");
    auto vocab_len = vocab.size();
    std::unordered_map<std::string, rowid_t> term_to_index_map;
    for (rowid_t i = 0; i < vocab_len; ++i) {
        term_to_index_map[vocab[i]] = i;
    }

    std::unordered_map<std::string, std::vector<rank_t>> results;
    while (query.executeStep()) {
        const std::string filename = query.getColumn(0);
        const std::string term = query.getColumn(1);
        const rank_t count = query.getColumn(2);

        if (term_to_index_map.contains(term)) {
            if (!results.contains(filename)) {
                results[filename] = std::vector<rank_t>(vocab_len);
            }
            auto word_idx = term_to_index_map[term];

            results[filename][word_idx] = count;
        }
    }

    return results;
}

std::unordered_map<std::string, std::vector<rank_t>>
get_tf_idf_per_file(Database &db,
                    const std::unordered_set<std::string> &stop_words)
{
    const rank_t no_of_files = db.execAndGet("SELECT COUNT(*) FROM zk");

    Statement query(
        db,
        "SELECT title as filename, term, COUNT(*) as count \n"
        "FROM zk_vocab_ins JOIN zk ON zk_vocab_ins.doc = zk.rowid\n"
        "WHERE col = 'body'\n"
        "GROUP BY filename, term \n"
        ";");

    auto vocab = get_vocab_words(db, stop_words);
    auto vocab_len = vocab.size();
    std::unordered_map<std::string, rowid_t> term_to_index_map;
    for (rowid_t i = 0; i < vocab_len; ++i) {
        term_to_index_map[vocab[i]] = i;
    }

    auto word_file_counts = get_file_counts_per_word(db, stop_words);

    std::unordered_map<std::string, std::vector<rank_t>> results;
    while (query.executeStep()) {
        const std::string filename = query.getColumn(0);
        const std::string term = query.getColumn(1);
        const rank_t count = query.getColumn(2);

        if (term_to_index_map.contains(term)) {
            if (!results.contains(filename)) {
                results[filename] = std::vector<rank_t>(vocab_len);
            }
            auto word_idx = term_to_index_map[term];

            auto word_file_cnt = word_file_counts[term];

            results[filename][word_idx] =
                count * std::log(no_of_files / word_file_cnt);
        }
    }

    return results;
}

std::unordered_map<std::string, rank_t>
get_file_counts_per_word(Database &db,
                         const std::unordered_set<std::string> &stop_words)
{
    Statement query(db,
                    "SELECT term, doc as count \n"
                    "FROM zk_vocab_col\n"
                    "WHERE col = 'body'\n"
                    ";");

    std::unordered_map<std::string, rank_t> results;
    while (query.executeStep()) {
        const std::string term = query.getColumn(0);
        const rank_t count = query.getColumn(1);

        if (!stop_words.contains(term)) {
            results[term] = count;
        }
    }

    return results;
}

std::unordered_map<std::string, rowid_t> get_file_ids(Database &db)
{
    Statement query(db, "SELECT rowid, title FROM zk;");
    std::unordered_map<std::string, rowid_t> return_val;
    while (query.executeStep()) {
        const rowid_db_get_t id = query.getColumn(0);
        std::string title = query.getColumn(1);
        return_val[std::move(title)] = static_cast<rowid_t>(id);
    }

    return return_val;
}

std::vector<std::pair<rowid_t, rowid_t>> get_links_as_id_pairs(Database &db)
{
    Statement query(db,
                    "SELECT origin_id, dest_id\n"
                    "FROM links\n"
                    "ORDER BY origin_id, dest_id ASC;");

    std::vector<std::pair<rowid_t, rowid_t>> results;
    while (query.executeStep()) {
        const rowid_db_get_t origin_id = query.getColumn(0);
        const rowid_db_get_t dest_id = query.getColumn(1);
        results.emplace_back(static_cast<rowid_t>(origin_id),
                             static_cast<rowid_t>(dest_id));
    }
    return results;
}

std::unordered_multimap<rowid_t, rowid_t> get_links_as_id_ummap(Database &db)
{
    Statement query(db,
                    "SELECT origin_id, dest_id\n"
                    "FROM links\n"
                    "ORDER BY origin_id, dest_id ASC;");

    std::unordered_multimap<rowid_t, rowid_t> results;
    while (query.executeStep()) {
        const rowid_db_get_t origin_id = query.getColumn(0);
        const rowid_db_get_t dest_id = query.getColumn(1);
        results.emplace(static_cast<rowid_t>(origin_id),
                        static_cast<rowid_t>(dest_id));
    }
    return results;
}

std::vector<std::pair<std::string, std::string>>
get_links_as_filename_pairs(Database &db)
{
    Statement query(db,
                    "SELECT o.title AS origin, d.title AS dest\n"
                    "FROM\n"
                    "    links \n"
                    "    JOIN zk AS o ON links.origin_id = o.rowid\n"
                    "    JOIN zk AS d ON links.dest_id = d.rowid\n"
                    "ORDER BY origin, dest ASC;");

    std::vector<std::pair<std::string, std::string>> results;
    while (query.executeStep()) {
        std::string origin_id = query.getColumn(0);
        std::string dest_id = query.getColumn(1);
        results.emplace_back(std::move(origin_id), std::move(dest_id));
    }
    return results;
}

std::string _n_param(size_t n)
{
    size_t string_size = (n - 1) * 3 + 1;
    std::string result;
    result.reserve(string_size);

    if (n > 0) {
        result.append("?"s);
        --n;
        for (size_t i = 0; i < n; ++i) {
            result.append(", ?"s);
        }
    }
    return result;
}

std::vector<std::string>
get_coexisting_tags(const std::unordered_set<tag_t> &tags, Database &db)
{
    auto number_of_tags = tags.size();

    if (number_of_tags < 1) {
        throw argument_error(
            "You must provide one or more tags to search for."s);
    }

    Statement query(db,
                    "SELECT DISTINCT term as tag\n"
                    "FROM zk_vocab_ins\n"
                    "WHERE col = 'tags'\n"
                    "    AND doc IN (\n"
                    "        SELECT DISTINCT doc\n"
                    "        FROM zk_vocab_ins\n"
                    "        WHERE col = 'tags'\n"
                    "            AND term IN ("
                        + _n_param(number_of_tags)
                        + ")\n"
                          ")\n"
                          "ORDER BY tag ASC\n"
                          ";");

    int bind_idx = 1;
    for (const auto &tag : tags) {
        query.bind(bind_idx, tag.get());
        ++bind_idx;
    }

    std::vector<std::string> results;
    while (query.executeStep()) {
        std::string tag = query.getColumn(0);
        if (!tags.contains(tag_t(tag))) {
            results.emplace_back(std::move(tag));
        }
    }
    return results;
}

std::vector<std::pair<db_cnt_t, std::string>>
get_file_link_tree(const filename_t &file, Database &db, size_t depth)
{
    using entry_t = std::pair<std::unordered_set<filename_t>, filename_t>;

    std::stack<entry_t> s;
    s.push({ {}, file });

    std::vector<std::pair<db_cnt_t, std::string>> results;

    while (!s.empty()) {
        auto [hist, f] = std::move(s.top());
        s.pop();

        // append result
        if (!hist.contains(f)) {
            auto level = hist.size();
            results.emplace_back(level, f.get());

            // push next
            if (depth == 0 || level < depth) {
                hist.insert(f);
                auto linked_files = list_links_in_file(f, db);
                for (auto &linked_file : linked_files) {
                    s.push({ hist, filename_t(std::move(linked_file.first)) });
                }
            }
        }
    }
    return results;
}

std::vector<std::string> list_tags_in_file(const filename_t &file,
                                           Database &db)
{
    Statement query(db,
                    "SELECT DISTINCT term\n"
                    "FROM zk_vocab_ins\n"
                    "WHERE col = 'tags' AND doc = (\n"
                    "        SELECT rowid FROM zk WHERE title = ?\n"
                    "    )\n"
                    "ORDER BY term ASC;");
    query.bind(1, file.get());

    std::vector<std::string> results;
    while (query.executeStep()) {
        results.emplace_back(query.getColumn(0));
    }

    return results;
}

std::vector<std::string> list_all_tags(Database &db)
{
    Statement query(db,
                    "SELECT DISTINCT term\n"
                    "FROM zk_vocab_ins\n"
                    "WHERE col = 'tags'"
                    "ORDER BY term ASC;");

    std::vector<std::string> results;
    while (query.executeStep()) {
        results.emplace_back(query.getColumn(0));
    }

    return results;
}

std::vector<std::string> list_files_containing_tag(const tag_t &tag,
                                                   Database &db)
{
    Statement query(db,
                    "SELECT title\n"
                    "FROM zk\n"
                    "WHERE zk MATCH ?\n"
                    "ORDER BY title ASC\n"
                    ";");
    query.bind(1, "tags : \""s + tag.get() + "\"");

    std::vector<std::string> results;
    while (query.executeStep()) {
        results.emplace_back(query.getColumn(0));
    }

    return results;
}

std::vector<std::pair<std::string, rowid_t>> get_tag_fileid_pairs(Database &db)
{
    Statement query(db,
                    "SELECT DISTINCT term, doc\n"
                    "FROM zk_vocab_ins\n"
                    "WHERE col = 'tags'"
                    "ORDER BY term ASC;");

    std::vector<std::pair<std::string, rowid_t>> results;
    while (query.executeStep()) {
        std::string tag = query.getColumn(0);
        rowid_db_get_t fileid = query.getColumn(1);
        results.emplace_back(std::move(tag), static_cast<rowid_t>(fileid));
    }

    return results;
}

std::unordered_multimap<std::string, rowid_t>
get_tag_fileid_ummap(Database &db)
{
    // TODO(r.andrej@gmail.com): change the return type to bidirectional
    // multimap
    Statement query(db,
                    "SELECT DISTINCT term, doc\n"
                    "FROM zk_vocab_ins\n"
                    "WHERE col = 'tags'"
                    "ORDER BY term ASC;");

    std::unordered_multimap<std::string, rowid_t> results;
    while (query.executeStep()) {
        std::string tag = query.getColumn(0);
        rowid_db_get_t fileid = query.getColumn(1);
        results.emplace(std::move(tag), static_cast<rowid_t>(fileid));
    }

    return results;
}

std::unordered_multimap<rowid_t, std::string>
get_fileid_tag_ummap(SQLite::Database &db)
{
    Statement query(db,
                    "SELECT DISTINCT term, doc\n"
                    "FROM zk_vocab_ins\n"
                    "WHERE col = 'tags'"
                    "ORDER BY term ASC;");

    std::unordered_multimap<rowid_t, std::string> results;
    while (query.executeStep()) {
        std::string tag = query.getColumn(0);
        rowid_db_get_t fileid = query.getColumn(1);
        results.emplace(static_cast<rowid_t>(fileid), std::move(tag));
    }

    return results;
}

std::vector<std::pair<db_cnt_t, std::string>>
get_file_tag_sibling_tree(const filename_t &file, Database &db, size_t depth)
{
    using file_or_tag_t = std::variant<filename_t, tag_t>;

    struct entry_t {
        file_or_tag_t file_or_tag;
        db_cnt_t level;
    };

    depth *= 2;

    // queue to determine at which level each tag should appear:
    // this first traversal determines the shallowest level each tag
    // appears at

    struct tag_level_t {
        db_cnt_t level;
        bool visited;
    };

    std::unordered_map<tag_t, tag_level_t> tag_levels;

    std::queue<entry_t> q;
    q.push({ file, 0 });

    std::unordered_map<filename_t, std::vector<std::string>> tag_cache;

    while (!q.empty()) {
        auto qe = std::move(q.front());
        q.pop();

        std::visit(
            overload {
                [&db, &q, &qe, &tag_levels, depth,
                 &tag_cache](const filename_t &f) {
                    auto tags = list_tags_in_file(f, db);
                    tag_cache[f] = tags;
                    if (depth == 0 || qe.level < depth) {
                        for (auto &tag : tags) {
                            if (!tag_levels.contains(tag_t(tag))) {
                                q.emplace(tag_t(tag), qe.level + 1);
                                tag_levels.insert({ tag_t(std::move(tag)),
                                                    { qe.level + 1, false } });
                            }
                        }
                    }
                },
                [&db, &q, &qe](const tag_t &tag) {
                    auto files = list_files_containing_tag(tag, db);
                    for (auto &f : files) {
                        q.emplace(filename_t(std::move(f)), qe.level + 1);
                    }
                } },
            qe.file_or_tag);
    }

    // stack to depth-first explore the tree

    std::stack<entry_t> s;
    s.push({ file, 0 });

    std::vector<std::pair<db_cnt_t, std::string>> results;

    while (!s.empty()) {
        auto e = std::move(s.top());
        s.pop();

        std::visit(
            overload {
                [&db, &e, &tag_levels, &results, depth, &s](const tag_t &tag) {
                    if (tag_levels[tag].level == e.level
                        && !tag_levels[tag].visited) {
                        // tag:

                        // 1. add to output array
                        results.emplace_back(e.level, "#"s + tag.get());

                        // 2. add all files containing it to stack for
                        // processing if they are at the correct level
                        if (depth == 0 || e.level <= depth) {
                            auto files = list_files_containing_tag(tag, db);
                            for (auto &f : files | std::views::reverse) {
                                s.emplace(filename_t(std::move(f)),
                                          e.level + 1);
                            }
                        }

                        // mark as visited
                        tag_levels[tag].visited = true;
                    }
                },

                [&db, &e, &results, depth, &s,
                 &tag_cache](const filename_t &f) {
                    // file:

                    // 1. add file to output array
                    results.emplace_back(e.level, f.get());

                    // 2. add all tags from file for processing
                    if (depth == 0 || e.level < depth) {
                        auto tags = (tag_cache.contains(f))
                            ? std::move(tag_cache[f])
                            : list_tags_in_file(f, db);
                        for (auto &tag : tags | std::views::reverse) {
                            s.emplace(tag_t(std::move(tag)), e.level + 1);
                        }
                    }
                } },
            e.file_or_tag);
    }
    return results;
}

} // namespace libzetk
