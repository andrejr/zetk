// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <set>
#include <string>
#include <utility>
#include <vector>

#include <unordered_map>

namespace libzetk {

bool compare_by_score(std::pair<std::string, double> lhs,
                      std::pair<std::string, double> rhs);

using SimilaritySet = std::set<std::pair<double, std::string>, std::greater<>>;

double cos_similarity_between_vectors(const std::vector<double> &a,
                                      const std::vector<double> &b);

SimilaritySet
find_files_similar_to_file(const std::string &file,
                           std::unordered_map<std::string, std::vector<double>>
                               &word_measures_per_file);

} // namespace libzetk
