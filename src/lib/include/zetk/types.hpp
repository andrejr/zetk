// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <compare>
#include <ctime>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>

#include <NamedType/named_type.hpp>
#include <unordered_map>
#include <unordered_set>

using namespace std::string_literals;

namespace libzetk {
using rowid_t = std::size_t;
using db_cnt_t = std::size_t;
using mtime_t = std::time_t;
using rank_t = double;

using LinkAndCount = std::vector<std::pair<std::string, db_cnt_t>>;
using LinkAndCountMap = std::unordered_map<std::string, db_cnt_t>;

using filename_t =
    fluent::NamedType<std::string, struct FilenameTag, fluent::Printable,
                      fluent::Addable, fluent::Hashable, fluent::Comparable>;
using tag_t =
    fluent::NamedType<std::string, struct TagTag, fluent::Printable,
                      fluent::Addable, fluent::Hashable, fluent::Comparable>;

struct FileMetadata {
    std::string title;
    rowid_t id;
    mtime_t mtime;

    auto operator<=>(const FileMetadata &) const = default;
};

class not_found_error : public std::runtime_error {
public:
    using std::runtime_error::runtime_error;
};

class argument_error : public std::runtime_error {
public:
    using std::runtime_error::runtime_error;
};

class FtsResult {
public:
    FtsResult(std::string filename, std::string body, std::string tags,
              double rank)
        : rank_(rank),
          filename_(std::move(filename)),
          body_(std::move(body)),
          tags_(std::move(tags))
    {
    }
    [[nodiscard]] const double &rank() const { return rank_; }
    [[nodiscard]] const std::string &filename() const { return filename_; }
    [[nodiscard]] const std::string &body() const { return body_; }
    [[nodiscard]] const std::string &tags() const { return tags_; }
    auto operator<=>(const FtsResult &) const = default;
    friend std::ostream &operator<<(std::ostream &os, const FtsResult &res)
    {
        return os << "{ filename: \""s << res.filename_ << R"(", body: ")"
                  << res.body_ << "\", rank: "s << res.rank_ << " }"s;
    }

private:
    double rank_;
    std::string filename_;
    std::string body_;
    std::string tags_;
};

} // namespace libzetk
