// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "zetk/types.hpp"

#include <SQLiteCpp/SQLiteCpp.h>
#include <boost/functional/hash.hpp>

using namespace std::string_literals;

namespace libzetk {

void output_dot(bool draw_tags, std::ostream &os, SQLite::Database &db);

using FileIdsOrTags = std::unordered_set<std::variant<rowid_t, tag_t>>;
using FileIds = std::unordered_set<rowid_t>;
using TagFileLinks = std::unordered_multimap<std::string, rowid_t>;

struct FileIdsAndLinks {
    using IdLink = std::pair<rowid_t, rowid_t>;
    using Links = std::unordered_set<IdLink, boost::hash<IdLink>>;

    FileIds fileids;
    Links links;

    auto operator<=>(const FileIdsAndLinks &) const = default;
};

struct TagsAndTagFileLinks {
    using Tags = std::unordered_set<std::string>;

    Tags tags;
    FileIds fileids;
    TagFileLinks tag_links;

    auto operator<=>(const TagsAndTagFileLinks &) const = default;
};

FileIdsAndLinks get_fileids_and_links_connected_to_file(const FileIds &fileids,
                                                        SQLite::Database &db,
                                                        size_t depth = 0U);

TagsAndTagFileLinks
get_tags_and_links_to_tags_from_files(const FileIdsOrTags &fileids_or_tags,
                                      SQLite::Database &db, size_t depth = 0U);

void output_dot_subgraph(const FileIdsOrTags &fileids_or_tags, bool draw_tags,
                         std::ostream &os, SQLite::Database &db,
                         size_t depth = 0U);

}
