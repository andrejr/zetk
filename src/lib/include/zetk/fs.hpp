// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#pragma once
#include <ctime>
#include <filesystem>
#include <string>

#include <boost/filesystem/path.hpp>
#include <string_view>

namespace libzetk {

std::string read_file(const std::filesystem::path &file_path);

time_t get_file_mtime(const std::filesystem::path &file_path);

std::filesystem::path
resolve_potential_symlink(std::filesystem::path potential_symlink);

std::filesystem::path boost_path_to_std_path(const boost::filesystem::path &p);

} // namespace libzetk
