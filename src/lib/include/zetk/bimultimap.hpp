// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <boost/bimap.hpp>
#include <boost/bimap/multiset_of.hpp>
#include <boost/bimap/unordered_multiset_of.hpp>

namespace libzetk {
template <typename L, typename R>
using unordered_bimultimap =
    boost::bimap<boost::bimaps::unordered_multiset_of<L>,
                 boost::bimaps::unordered_multiset_of<R>>;

template <typename L, typename R>
using bimultimap =
    boost::bimap<boost::bimaps::multiset_of<L>, boost::bimaps::multiset_of<R>>;
}
