// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "zetk/db.hpp"

#include <memory>
#include <utility>

#include <libfswatch/c++/monitor.hpp>

namespace libzetk {
struct FswContext {
public:
    FswContext() = default;
    ~FswContext() = default;
    FswContext(FswContext &&) = default;
    FswContext(const FswContext &) = delete;
    FswContext &operator=(FswContext &&) = delete;
    FswContext &operator=(const FswContext &rhs) = delete;

private:
    FswContext(path folder_path, path db_path)
        : folder_path_(std::move(folder_path)), db_path_(std::move(db_path))
    {
    }
    const path folder_path_;
    const path db_path_;

    friend std::pair<std::unique_ptr<fsw::monitor>,
                     std::unique_ptr<FswContext>>
    make_monitor(const path &folder_path, const path &db_path,
                 double latency_seconds);
};

constexpr double default_fswatch_latency = 2.0;

std::pair<std::unique_ptr<fsw::monitor>, std::unique_ptr<FswContext>>
make_monitor(const path &folder_path, const path &db_path,
             double latency_seconds = default_fswatch_latency);
} // namespace libzetk
