// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <functional>
#include <utility>

namespace libzetk {
struct scope_exit {
    explicit scope_exit(std::function<void(void)> f) noexcept
        : f_(std::move(f))
    {
    }
    ~scope_exit() { f_(); }

private:
    std::function<void(void)> f_;
};
}
