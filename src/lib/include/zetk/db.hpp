// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "zetk/types.hpp"

#include <filesystem>
#include <optional>
#include <string>
#include <utility>
#include <vector>

#include <SQLiteCpp/SQLiteCpp.h>
#include <unordered_map>
#include <unordered_set>

using std::filesystem::path;

namespace libzetk {

SQLite::Database create_db(const path &db_path);

SQLite::Database open_db(const path &db_path);

void add_file_to_db(const filename_t &title, const std::string &body,
                    const tag_t &tags, mtime_t mtime, SQLite::Database &db);

std::vector<std::pair<std::string, mtime_t>>
list_files_in_db(SQLite::Database &db);

std::unordered_map<std::string, mtime_t>
get_files_in_db_map(SQLite::Database &db);

std::optional<std::string> get_file_contents_from_db(const filename_t &file,
                                                     SQLite::Database &db);

std::optional<FileMetadata> get_file_metadata(filename_t title,
                                              SQLite::Database &db);

std::string wrap_fts_query(const std::string &query);

void delete_file_from_db(const filename_t &title, SQLite::Database &db);

std::optional<rowid_t>
get_unique_file_id_from_prefix(const filename_t &filename,
                               SQLite::Database &db);
std::optional<std::string>
get_unique_filename_from_prefix(const filename_t &filename,
                                SQLite::Database &db);

void add_or_update_links_for_file(const filename_t &title,
                                  const LinkAndCountMap &links,
                                  SQLite::Database &db);

std::vector<std::pair<std::string, db_cnt_t>>
list_tags_by_freq(SQLite::Database &db);

std::vector<std::pair<std::string, db_cnt_t>>
list_files_linking_to_file(const filename_t &destination_file,
                           SQLite::Database &db);

std::vector<std::pair<std::string, db_cnt_t>>
list_links_in_file(const filename_t &origin_file, SQLite::Database &db);

std::vector<FtsResult>
fulltext_search_all_files(const std::string &search, SQLite::Database &db,
                          const std::string_view &highlight_start = "",
                          const std::string_view &highlight_stop = "");

std::optional<FtsResult>
fulltext_search_in_file(const std::string &search, const filename_t &file,
                        SQLite::Database &db,
                        const std::string_view &highlight_start = "",
                        const std::string_view &highlight_stop = "");

std::vector<std::pair<std::string, rowid_t>>
list_filenames_and_db_ids(SQLite::Database &db);

std::vector<std::string>
get_vocab_words(SQLite::Database &db,
                const std::unordered_set<std::string> &stop_words = {},
                const std::string &column = "body");

std::unordered_map<std::string, std::vector<rank_t>>
get_word_frequencies_per_file(
    SQLite::Database &db,
    const std::unordered_set<std::string> &stop_words = {});

std::unordered_map<std::string, std::vector<rank_t>>
get_tag_incidence_per_file(
    SQLite::Database &db,
    const std::unordered_set<std::string> &ignored_tags = {});

std::unordered_map<std::string, std::vector<rank_t>>
get_tf_idf_per_file(SQLite::Database &db,
                    const std::unordered_set<std::string> &stop_words = {});

std::unordered_map<std::string, rank_t> get_file_counts_per_word(
    SQLite::Database &db,
    const std::unordered_set<std::string> &stop_words = {});

std::vector<std::pair<rowid_t, rowid_t>>
get_links_as_id_pairs(SQLite::Database &db);

std::unordered_multimap<rowid_t, rowid_t>
get_links_as_id_ummap(SQLite::Database &db);

std::vector<std::pair<std::string, std::string>>
get_links_as_filename_pairs(SQLite::Database &db);

std::vector<std::string>
get_coexisting_tags(const std::unordered_set<tag_t> &tags,
                    SQLite::Database &db);

std::vector<std::pair<db_cnt_t, std::string>>
get_file_link_tree(const filename_t &file, SQLite::Database &db,
                   size_t depth = 0);

std::vector<std::string> list_tags_in_file(const filename_t &file,
                                           SQLite::Database &db);

std::vector<std::string> list_files_containing_tag(const tag_t &tag,
                                                   SQLite::Database &db);

std::vector<std::pair<db_cnt_t, std::string>>
get_file_tag_sibling_tree(const filename_t &file, SQLite::Database &db,
                          size_t depth = 0);

std::vector<std::pair<std::string, rowid_t>>
get_tag_fileid_pairs(SQLite::Database &db);

std::unordered_multimap<std::string, rowid_t>
get_tag_fileid_ummap(SQLite::Database &db);

std::unordered_multimap<rowid_t, std::string>
get_fileid_tag_ummap(SQLite::Database &db);

std::vector<std::string> list_all_tags(SQLite::Database &db);

} // namespace libzetk
