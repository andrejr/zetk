// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <filesystem>
#include <fstream>
#include <utility>

#include <boost/interprocess/sync/file_lock.hpp>
#include <zetk/db.hpp>
#include <zetk/env.hpp>
#include <zetk/fs.hpp>
#include <zetk/home.hpp>

namespace libzetk {

struct ZkEnvVars {
    std::filesystem::path zk_folder;
    std::filesystem::path zk_db_path = zk_folder / "index.db";
};

inline SQLite::Database open_or_create_db(const path &db_path)
{
    auto db = (!std::filesystem::is_regular_file(db_path)) ? create_db(db_path)
                                                           : open_db(db_path);

    // TODO(r.andrej@gmail.com): Catch permission errors (for open and close)
    // and catch folder not existing when creating a database.
    // Rethrow as human-readable errors.
    return db;
}

inline ZkEnvVars get_zk_env_vars()
{
    auto zk_path = get_env_var("ZK_PATH");
    if (!zk_path.has_value()) {
        throw env_var_error(
            "Environment variable ZK_PATH is not defined. "s
            "It must be set to the path containing your zettelkasten "s
            "markdown files in order for this tool to work."s);
    }
    auto zk_res_path = resolve_potential_symlink(resolve_home(*zk_path));
    if (!std::filesystem::is_directory(zk_res_path)) {
        throw env_var_error(
            "Environment variable ZK_PATH does not resolve to a folder. "s
            "It must be set to the path of a folder containing your "s
            "zettelkasten markdown files in order for this tool to work."s);
    }

    ZkEnvVars res = { zk_res_path };

    auto zk_db_path = get_env_var("ZK_DB_PATH");
    if (zk_db_path.has_value()) {
        res.zk_db_path = resolve_potential_symlink(resolve_home(*zk_db_path));
    }

    return res;
}

inline bool lockfile_exists(const path &lockfile_path)
{
    if (!std::filesystem::is_regular_file(lockfile_path)) {
        return false;
    }
    boost::interprocess::file_lock lockfile(lockfile_path.c_str());
    if (lockfile.try_lock()) {
        lockfile.unlock();
        std::filesystem::remove(lockfile_path);
        return false;
    }
    return true;
}

inline boost::interprocess::file_lock
create_lockfile(const path &lockfile_path)
{
    if (!std::filesystem::is_regular_file(lockfile_path)) {
        std::ofstream out(lockfile_path);
    }
    return { lockfile_path.c_str() };
}

} // namespace libzetk
