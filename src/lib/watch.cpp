// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#include "zetk/watch.hpp"

#include "zetk/db.hpp"
#include "zetk/init.hpp"
#include "zetk/read.hpp"

#include <execution>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include <boost/log/trivial.hpp>
#include <libfswatch/c++/event.hpp>
#include <libfswatch/c++/monitor_factory.hpp>

using namespace std::string_literals;

namespace libzetk {

std::pair<std::unique_ptr<fsw::monitor>, std::unique_ptr<FswContext>>
make_monitor(const path &folder_path, const path &db_path,
             double latency_seconds)
{
    std::unique_ptr<FswContext> context { new FswContext { folder_path,
                                                           db_path } };

    auto handler = [](const std::vector<fsw::event> &events,
                      void *raw_context) {
        auto *ctx = static_cast<FswContext *>(raw_context);
        const auto &db_pth = ctx->db_path_;

        auto db = open_or_create_db(db_pth);

        // this is intentionally sequential (and single-threaded)
        // copying a file triggers multiple events, for example, and we don't
        // want them to be concurrently processed
        for (const fsw::event &evt : events) {
            auto flag = evt.get_flags()[0];
            const std::filesystem::path full_path = evt.get_path();
            switch (flag) {
            case Created:
                read_file_into_db(full_path, db, false);
                break;
            case Removed:
                delete_file_from_db(filename_t(full_path), db);
                // TODO(r.andrej@gmail.com): check if file is properly deleted
                BOOST_LOG_TRIVIAL(info)
                    << "Removed the DB entry for " << full_path.filename()
                    << " because the file no longer exists.";
                break;
            case Updated:
                read_file_into_db(full_path, db, true);
                break;
            default:
                break;
            }
        }
    };

    std::unique_ptr<fsw::monitor> monitor {
        fsw::monitor_factory::create_monitor(
            fsw_monitor_type::system_default_monitor_type, { folder_path },
            handler, context.get())
    };
    monitor->set_latency(latency_seconds);
    monitor->set_filters(
        { { .text = "\\.md$",
            .type = fsw_filter_type::filter_include,
            .case_sensitive = false,
            .extended = false },
          { .text = static_cast<std::string>(folder_path) + "/.*"s,
            .type = fsw_filter_type::filter_exclude,
            .case_sensitive = false,
            .extended = false } });
    monitor->set_recursive(false);
    monitor->add_event_type_filter({ fsw_event_flag::Created });
    monitor->add_event_type_filter({ fsw_event_flag::Updated });
    monitor->add_event_type_filter({ fsw_event_flag::Removed });

    return { std::move(monitor), std::move(context) };
}
} // namespace libzetk
