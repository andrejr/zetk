// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "log.hpp"
#include "zetk/db.hpp"
#include "zetk/init.hpp"
#include "zetk/read.hpp"

#include <utility>

namespace libzetk {

constexpr std::string_view ansi_hl_start = "\x1b[0;41m";
constexpr std::string_view ansi_hl_end = "\x1b[0m";

constexpr std::string_view lockfile_name = "daemon.lock";

struct EnvAndDb {
    SQLite::Database db;
    path zk_dir_path;
    path db_path;
};

inline EnvAndDb
init_cli(std::unordered_set<std::string> ignored_log_scopes = { "db_sync" },
         bool sync_db_if_no_lockfile = true)
{
    setup_cli_logging(std::move(ignored_log_scopes));
    auto [zk_dir_path, db_path] = get_zk_env_vars();
    auto db = open_or_create_db(db_path);

    // If the lockfile exists, this means that the daemon is running and we
    // don't have to refresh the database from the filesystem - the daemon is
    // doing that continuously.
    // If the lockfile doesn't exist, we will update the db from the
    // filesystem.
    if (!lockfile_exists(zk_dir_path / lockfile_name)
        && sync_db_if_no_lockfile) {
        synchronize_folder_and_db(zk_dir_path, db);
    }

    return { std::move(db), std::move(zk_dir_path), std::move(db_path) };
}

class file_missing_error : public std::runtime_error {
public:
    using std::runtime_error::runtime_error;
};

inline std::string resolve_file_prefix(const std::string &prefix,
                                       SQLite::Database &db)
{

    if (auto resolved_filename =
            get_unique_filename_from_prefix(filename_t(prefix), db)) {
        return resolved_filename.value();
    }
    throw file_missing_error(
        "Prefix '"s + prefix
        + "' is either not unique or does not relate to an existing file"s);
}

} // namespace libzetk
