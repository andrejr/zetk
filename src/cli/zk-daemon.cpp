// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#include "cli_init.hpp"
#include "log.hpp"
#include "zetk/db.hpp"
#include "zetk/env.hpp"
#include "zetk/home.hpp"
#include "zetk/init.hpp"
#include "zetk/read.hpp"
#include "zetk/scope_exit.hpp"
#include "zetk/version.hpp"
#include "zetk/watch.hpp"

#include <csignal>
#include <iostream>
#include <memory>
#include <stdexcept>

#include <args.hxx>

using namespace std::string_literals;
using namespace libzetk;

static std::vector<std::function<void(int)>> signal_handlers;

void signal_handler(int signal)
{
    for (auto &handler : signal_handlers) {
        handler(signal);
    }
    exit(1);
}

void setup_signal_handling()
{
    std::signal(SIGABRT, signal_handler);
    std::signal(SIGFPE, signal_handler);
    std::signal(SIGILL, signal_handler);
    std::signal(SIGINT, signal_handler);
    std::signal(SIGTERM, signal_handler);
};

int main(int argc, char **argv)
{
    args::ArgumentParser parser(
        "Watch for file creation / modification / deletion in $ZK_PATH and "s
        "update the zettelkasten fulltext/link database to match the state "s
        "of the $ZK_PATH folder in the filesystem."s);

    args::HelpFlag help(parser, "help", "Prints help information",
                        { 'h', "help" });
    args::HelpFlag version(parser, "version",
                           "Prints the version number and exits",
                           { 'v', "version" });

    try {
        parser.ParseCLI(argc, argv);

        setup_signal_handling();
        setup_cli_logging();

        auto [zk_dir_path, db_path] = get_zk_env_vars();

        auto lockfile_path = zk_dir_path / lockfile_name;
        auto lockfile = create_lockfile(lockfile_path);
        if (!lockfile.try_lock()) {
            throw std::runtime_error("Unable to create/obtain lockfile `"s
                                     + lockfile_path.string() + "`"s);
        }
        auto lock_deleter { std::make_unique<scope_exit>(
            [&lockfile_path, &lockfile]() {
                lockfile.unlock();
                std::filesystem::remove(lockfile_path);
            }) };
        signal_handlers.emplace_back(
            [&lock_deleter](int) { lock_deleter.reset(); });

        // synchronize the folder once before starting the monitor
        {
            auto db = open_or_create_db(db_path);
            synchronize_folder_and_db(zk_dir_path, db);
        }

        auto [monitor, context] = make_monitor(zk_dir_path, db_path);
        BOOST_LOG_TRIVIAL(info)
            << "Monitoring folder " << zk_dir_path
            << " for changes and updating the database " << db_path << ".\n";
        monitor->start();
    } catch (const args::Completion &e) {
        std::cout << e.what();
        return 0;
    } catch (const args::Help &e) {
        if (*(e.what()) == 'h') {
            std::cout << parser;
        } else {
            std::cout << "gccdiag v" << version_string() << "\n";
        }
        return 0;
    } catch (const std::runtime_error &e) {
        std::cerr << e.what();
        return 2;
    }

    return 0;
}
