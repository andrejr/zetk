// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#include "cli_init.hpp"
#include "log.hpp"
#include "zetk/db.hpp"
#include "zetk/env.hpp"
#include "zetk/home.hpp"
#include "zetk/init.hpp"
#include "zetk/read.hpp"
#include "zetk/version.hpp"

#include <iostream>
#include <sstream>

#include <args.hxx>

using namespace std::string_literals;
using namespace libzetk;

int main(int argc, char **argv)
{
    args::ArgumentParser parser(
        "Full-text search of the Zettelkasten.\n\n"s
        "This utility has two different modes, "s
        "depending on whether the --file/-f flag "s
        "is provided or not.\n\n"s
        "If --file/-f isnt't provided, the whole "s
        "Zettelkasten is searched, and names of files matching "s
        "the query are printed line by line.\n\n"s
        "If a filename was provided with --file/-f, its contents are printed "s
        "with the keywords highlighted."s);

    args::PositionalList<std::string> search(
        parser, "search-query"s,
        "query to search for in zettelkasten / single file"s,
        args::Options::None | args::Options::Required);
    args::ValueFlag<path> file(
        parser, "file",
        "search only inside this file, print results with highlighted query"s,
        { 'f', "file"s });
    args::Flag tags_arg(
        parser, "tags",
        "search only tags, intead of the whole file (don't prefix tagswith #)"s,
        { 't', "tags"s });
    args::Flag print0(parser, "print0",
                      "use null character as delimiter instead of newline"s,
                      { '0', "print0"s });
    args::HelpFlag help(parser, "help", "print this help and exit"s,
                        { 'h', "help"s });
    args::HelpFlag version(parser, "version"s,
                           "output version information and exit"s,
                           { 'v', "version"s });

    try {
        auto [db, zk_dir_path, db_path] = init_cli();

        parser.ParseCLI(argc, argv);

        std::ostringstream sqss;

        if (tags_arg) {
            sqss << "tags: ";
        }
        for (bool first = true; const auto &term : args::get(search)) {
            if (first) {
                first = false;
            } else {
                sqss << " "s;
            }
            sqss << wrap_fts_query(term);
        }

        auto search_query = sqss.str();
        auto delimiter = print0 ? '\0' : '\n';

        if (file) {
            // if the filename arg was given
            auto filename =
                filename_t(resolve_file_prefix(args::get(file), db));

            // if file is not found, throw error: it only makes sense to search
            // inside existing files
            auto file_in_db = get_file_metadata(filename, db);
            if (!file_in_db) {
                throw file_missing_error("File \"" + filename.get()
                                         + "\" doesn't exist in DB.");
            }

            // if there is a search string, search inside said file
            auto res = fulltext_search_in_file(search_query, filename, db,
                                               ansi_hl_start, ansi_hl_end);
            if (res) {
                std::cout << res->body() << delimiter;
            } else {
                return 1;
            }
        } else {
            // if there's no filename arg

            // FTS the query in the DB and print the files containing matches
            auto res = fulltext_search_all_files(search_query, db,
                                                 ansi_hl_start, ansi_hl_end);
            if (!res.empty()) {
                for (const auto &entry : res) {
                    std::cout << entry.filename() << delimiter;
                }
            } else {
                return 1;
            }
        }
    } catch (const args::Completion &e) {
        std::cout << e.what();
        return 0;
    } catch (const args::Help &e) {
        if (*(e.what()) == 'h') {
            std::cout << parser;
        } else {
            std::cout << "gccdiag v" << version_string() << "\n";
        }
        return 0;
    } catch (const std::runtime_error &e) {
        std::cerr << e.what();
        return 2;
    }

    return 0;
}
