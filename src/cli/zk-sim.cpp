// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#include "cli_init.hpp"
#include "log.hpp"
#include "zetk/cos_similarity.hpp"
#include "zetk/db.hpp"
#include "zetk/env.hpp"
#include "zetk/fs.hpp"
#include "zetk/home.hpp"
#include "zetk/init.hpp"
#include "zetk/read.hpp"
#include "zetk/version.hpp"

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <set>

#include <args.hxx>
#include <boost/algorithm/string.hpp>
#include <boost/tokenizer.hpp>

using namespace std::string_literals;
using namespace libzetk;

int main(int argc, char **argv)
{
    args::ArgumentParser parser(
        "Prints the argument file's similarity with other documents in the "s
        "Zettelkasten in descending order (more similar docs first).\n"s
        "Cosine similarity is used, with either TF-IDF vectorization (for "s
        "text) or simple tag-presence testing (for tags).\n"s
        "If the environment variable ZK_STOPWORDS contains a path to a text "s
        "file with (whitespace delimited) words, these words will be used as "s
        "'stopwords': they will be excluded during vectorization and won't "s
        "affect the similarity score between files."s);

    args::Positional<path> file(parser, "file",
                                "File to search similar files to"s,
                                args::Options::Required);

    args::Flag tags_arg(
        parser, "tags",
        "calculate the similarity by tag occurence similarity (not counting) "s
        "instead"s,
        { 't', "tags"s });
    args::Flag score(
        parser, "freq",
        "Print file's similarity score before filename "s
        "(0 means completely dissimilar, 1 means maximally similar)."s,
        { 's', "score"s });
    args::Flag print0(parser, "print0",
                      "use null character as delimiter instead of newline"s,
                      { '0', "print0"s });
    args::HelpFlag help(parser, "help", "print this help and exit"s,
                        { 'h', "help"s });
    args::HelpFlag version(parser, "version"s,
                           "output version information and exit"s,
                           { 'v', "version"s });

    try {
        auto [db, zk_dir_path, db_path] = init_cli();

        parser.ParseCLI(argc, argv);

        const auto delimiter = print0 ? '\0' : '\n';

        auto filename = resolve_file_prefix(args::get(file), db);

        std::unordered_set<std::string> stopwords;

        auto zk_stopwords_path = get_env_var("ZK_STOPWORDS");
        if (zk_stopwords_path) {
            auto zk_stopwords_resolved_path =
                resolve_potential_symlink(resolve_home(*zk_stopwords_path));
            if (std::filesystem::is_regular_file(zk_stopwords_resolved_path)) {
                auto stopwords_string = read_file(zk_stopwords_resolved_path);
                boost::tokenizer tok { stopwords_string };
                std::transform(tok.begin(), tok.end(),
                               std::inserter(stopwords, stopwords.begin()),
                               [](const std::string &token) {
                                   return boost::algorithm::to_lower_copy(
                                       token);
                               });
            } else {
                throw file_missing_error(
                    "Unable to open file '"s
                    + zk_stopwords_resolved_path.string()
                    + "', given as stopword list through env var "s
                      "$ZK_STOPWORDS"s);
            }
        }

        auto word_measures_per_file = tags_arg
            ? get_tag_incidence_per_file(db)
            : get_tf_idf_per_file(db, stopwords);

        auto results =
            find_files_similar_to_file(filename, word_measures_per_file);

        if (!results.empty()) {
            std::cout << std::fixed << std::setprecision(4);
            for (const auto &[curr_score, title] : results) {
                if (score) {
                    std::cout << curr_score << "\t";
                }
                std::cout << title << delimiter;
            }
        } else {
            return 1;
        }
    } catch (const args::Completion &e) {
        std::cout << e.what();
        return 0;
    } catch (const args::Help &e) {
        if (*(e.what()) == 'h') {
            std::cout << parser;
        } else {
            std::cout << "gccdiag v" << version_string() << "\n";
        }
        return 0;
    } catch (const std::runtime_error &e) {
        std::cerr << e.what();
        return 2;
    }

    return 0;
}
