// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#include "cli_init.hpp"
#include "log.hpp"
#include "zetk/db.hpp"
#include "zetk/env.hpp"
#include "zetk/graphviz.hpp"
#include "zetk/home.hpp"
#include "zetk/init.hpp"
#include "zetk/read.hpp"
#include "zetk/version.hpp"

#include <iostream>

#include <args.hxx>
#include <boost/algorithm/string.hpp>

using namespace std::string_literals;
using namespace libzetk;

int main(int argc, char **argv)
{
    args::ArgumentParser parser(
        "Prints to stdout graphviz *dot* code representing the link and tag "s
        "graph of your zettelkasten.\n"s
        "By default, only direct links are shown, but you may use the "s
        "-t/--tags flag to show tags and their links in the graph.\n"s
        "If you provide the 'filenames_or_tags' args, the graph will be limited to "s
        "files (and potentially tags) connected to those files."s);

    args::PositionalList<std::string> filenames_or_tags(
        parser, "filenames or tags"s,
        "only draw files / tags connected to these files"s);
    args::Flag tags_arg(
        parser, "tags",
        "also draw tags (automatically enabled if you pass a tag among "s
        "`filenames_or_tags`)"s,
        { 't', "tags"s });
    args::ValueFlag<size_t> depth_arg(
        parser, "depth",
        "draw up to certain depth, 0 means unlimited (which is the default); "s
        "only makes sense when filenames or tags are provided"s,
        { 'd', "depth"s }, 0);
    args::HelpFlag help(parser, "help", "print this help and exit"s,
                        { 'h', "help"s });
    args::HelpFlag version(parser, "version"s,
                           "output version information and exit"s,
                           { 'v', "version"s });

    try {
        auto [db, zk_dir_path, db_path] = init_cli();

        parser.ParseCLI(argc, argv);

        auto tags = args::get(tags_arg);
        size_t depth = args::get(depth_arg);

        if (auto [filename_or_tag_list, tag_encountered] =
                std::pair(args::get(filenames_or_tags), false);
            !filename_or_tag_list.empty()) {

            FileIdsOrTags fileids_or_tags;
            for (const auto &filename_or_tag : filename_or_tag_list) {
                if (boost::algorithm::starts_with(filename_or_tag, "#"s)) {
                    auto tag_without_pound =
                        filename_or_tag.substr(1, filename_or_tag.size() - 1);
                    if (auto res = fulltext_search_all_files(
                            "tags: \"" + tag_without_pound + "\"", db);
                        !res.empty()) {

                        fileids_or_tags.insert(tag_t { tag_without_pound });
                        tag_encountered = true;
                    } else {
                        throw not_found_error(
                            "Tag '"s + filename_or_tag
                            + "' not found in the database"s);
                    }
                } else {
                    if (auto metadata =
                            get_file_metadata(filename_t(resolve_file_prefix(
                                                  filename_or_tag, db)),
                                              db)) {
                        fileids_or_tags.insert(metadata->id);
                    } else {
                        throw not_found_error(
                            "File '"s + filename_or_tag
                            + "' not found in the database"s);
                    }
                }
            }
            output_dot_subgraph(fileids_or_tags, tags || tag_encountered,
                                std::cout, db, depth);
        } else {
            output_dot(tags, std::cout, db);
        }
    } catch (const args::Completion &e) {
        std::cout << e.what();
        return 0;
    } catch (const args::Help &e) {
        if (*(e.what()) == 'h') {
            std::cout << parser;
        } else {
            std::cout << "gccdiag v" << version_string() << "\n";
        }
        return 0;
    } catch (const std::runtime_error &e) {
        std::cerr << e.what();
        return 2;
    }

    return 0;
}
