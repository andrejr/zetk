// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#include "cli_init.hpp"
#include "log.hpp"
#include "zetk/db.hpp"
#include "zetk/env.hpp"
#include "zetk/home.hpp"
#include "zetk/read.hpp"
#include "zetk/version.hpp"
#include "zetk/watch.hpp"

#include <iostream>

#include <args.hxx>

using namespace std::string_literals;
using namespace libzetk;

int main(int argc, char **argv)
{
    args::ArgumentParser parser(
        "Prints a tree of files connected to a given file by links/tags."s);

    args::Positional<path> file(parser, "file", "File used as root"s,
                                args::Options::Required);

    args::Flag tags(
        parser, "tags",
        "print a tree of files connected to the given file by tags instead of"s
        "links"s,
        { 't', "tags"s });

    args::ValueFlag<size_t> depth(
        parser, "depth",
        "only print tree up to certain depth (use 0 for full depth, which is "
        "the default)"s,
        { 'd', "depth"s });

    args::Flag print_depth(
        parser, "print_depth"s,
        "print depth in tree instead of printing tab spacing."s,
        { 'p', "print-depth"s });

    args::Flag print0(parser, "print0",
                      "use null character as delimiter instead of newline "s
                      "(only works with -p/--print-depth)"s,
                      { '0', "print0"s });

    args::HelpFlag help(parser, "help", "print this help and exit"s,
                        { 'h', "help"s });
    args::HelpFlag version(parser, "version"s,
                           "output version information and exit"s,
                           { 'v', "version"s });

    try {
        auto [db, zk_dir_path, db_path] = init_cli();
        parser.ParseCLI(argc, argv);

        auto filename = filename_t(resolve_file_prefix(args::get(file), db));
        size_t depth_val = (depth) ? args::get(depth) : 0;

        auto delimiter = print0 ? '\0' : '\n';

        auto tree = (tags) ? get_file_tag_sibling_tree(filename, db, depth_val)
                           : get_file_link_tree(filename, db, depth_val);

        if (!tree.empty()) {
            std::cout << std::fixed << std::setprecision(4);
            if (print_depth) {
                for (const auto &[cdepth, cfileortag] : tree) {
                    std::cout << cdepth << "\t" << cfileortag << delimiter;
                }
            } else {
                for (const auto &[cdepth, cfileortag] : tree) {
                    for (size_t i = 0; i < cdepth; ++i) {
                        std::cout << "\t";
                    }
                    std::cout << cfileortag << "\n";
                }
            }
        } else {
            return 1;
        }

    } catch (const args::Completion &e) {
        std::cout << e.what();
        return 0;
    } catch (const args::Help &e) {
        if (*(e.what()) == 'h') {
            std::cout << parser;
        } else {
            std::cout << "gccdiag v" << version_string() << "\n";
        }
        return 0;
    } catch (const std::runtime_error &e) {
        std::cerr << e.what();
        return 2;
    }

    return 0;
}
