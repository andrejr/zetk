// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#include "cli_init.hpp"
#include "log.hpp"
#include "zetk/db.hpp"
#include "zetk/env.hpp"
#include "zetk/home.hpp"
#include "zetk/init.hpp"
#include "zetk/read.hpp"
#include "zetk/version.hpp"

#include <iostream>

#include <args.hxx>

using namespace std::string_literals;
using namespace libzetk;

int main(int argc, char **argv)
{
    args::ArgumentParser parser(
        "Update the zettelkasten fulltext/link database to match the state "s
        "of the $ZK_PATH folder in the filesystem."s);

    args::HelpFlag help(parser, "help", "Prints help information",
                        { 'h', "help" });
    args::HelpFlag version(parser, "version",
                           "Prints the version number and exits",
                           { 'v', "version" });

    try {
        parser.ParseCLI(argc, argv);

        auto [db, zk_dir_path, db_path] = init_cli({}, false);

        BOOST_LOG_TRIVIAL(info)
            << "Scanning folder " << zk_dir_path
            << " for changes and updating the database " << db_path << "...";

        synchronize_folder_and_db(zk_dir_path, db);

        BOOST_LOG_TRIVIAL(info) << "Done.";

    } catch (const args::Completion &e) {
        std::cout << e.what();
        return 0;
    } catch (const args::Help &e) {
        if (*(e.what()) == 'h') {
            std::cout << parser;
        } else {
            std::cout << "gccdiag v" << version_string() << "\n";
        }
        return 0;
    } catch (const std::runtime_error &e) {
        std::cerr << e.what();
        return 2;
    }

    return 0;
}
