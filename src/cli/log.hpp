// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <unordered_set>

namespace libzetk {
BOOST_LOG_ATTRIBUTE_KEYWORD(my_named_scope, "Scope",
                            boost::log::attributes::named_scope::value_type)

inline void
setup_cli_logging(std::unordered_set<std::string> ignored_scopes = {})
{
    namespace logging = boost::log;
    namespace attrs = boost::log::attributes;
    /* namespace expr = boost::log::expressions; */

    logging::core::get()->add_global_attribute(
        "Scope", logging::attributes::named_scope());

    logging::core::get()->set_filter(
        [ignored_scopes = std::move(ignored_scopes)](
            const logging::attribute_value_set &attr_set) {
            const bool severity_ok =
                attr_set["Severity"]
                    .extract<logging::trivial::severity_level>()
                >= logging::trivial::info;
            const auto scopes =
                attr_set["Scope"].extract<attrs::named_scope_list>();
            /* const auto scopes = */
            /*     expr::attr<attrs::named_scope_list>("Scope").or_none(); */

            const bool scope_ignored = (scopes.empty())
                ? false
                : std::ranges::any_of(
                    scopes.get(),
                    [&ignored_scopes](const attrs::named_scope_entry &scope) {
                        return ignored_scopes.contains(scope.scope_name.str());
                    });

            return severity_ok && !scope_ignored;
        });

    boost::log::formatter log_fmt = boost::log::expressions::format("%1%")
        % boost::log::expressions::smessage;

    auto console_sink = boost::log::add_console_log(std::clog);
    console_sink->set_formatter(log_fmt);
}

} // namespace libzetk
