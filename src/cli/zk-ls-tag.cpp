// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#include "cli_init.hpp"
#include "log.hpp"
#include "zetk/db.hpp"
#include "zetk/env.hpp"
#include "zetk/home.hpp"
#include "zetk/init.hpp"
#include "zetk/read.hpp"
#include "zetk/version.hpp"

#include <iostream>

#include <args.hxx>
#include <boost/algorithm/string.hpp>

using namespace std::string_literals;
using namespace libzetk;

int main(int argc, char **argv)
{
    args::ArgumentParser parser(
        "List all tags appearing in the Zettelkasten by their frequency "s
        "in files."s);

    args::Flag freq(parser, "freq", "Print tag's frequency before tag."s,
                    { 'f', "freq"s });
    args::Flag print0(parser, "print0",
                      "use null character as delimiter instead of newline"s,
                      { '0', "print0"s });
    args::HelpFlag help(parser, "help", "print this help and exit"s,
                        { 'h', "help"s });
    args::HelpFlag version(parser, "version"s,
                           "output version information and exit"s,
                           { 'v', "version"s });

    try {
        auto [db, zk_dir_path, db_path] = init_cli();

        parser.ParseCLI(argc, argv);

        auto delimiter = print0 ? '\0' : '\n';

        auto tags = list_tags_by_freq(db);

        for (const auto &[tag, count] : tags) {
            if (freq) {
                std::cout << count << "\t";
            }
            std::cout << "#" << tag << delimiter;
        }
    } catch (const args::Completion &e) {
        std::cout << e.what();
        return 0;
    } catch (const args::Help &e) {
        if (*(e.what()) == 'h') {
            std::cout << parser;
        } else {
            std::cout << "gccdiag v" << version_string() << "\n";
        }
        return 0;
    } catch (const std::runtime_error &e) {
        std::cerr << e.what();
        return 2;
    }

    return 0;
}
