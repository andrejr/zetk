// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#include "zetk/parser.hpp"

#include <string>

#include <gtest/gtest.h>

using std::string;

TEST(TagsTest, Tags)
{
    string input { "word #tag1 word #2tag-2  #interr,upted#tag3#tag4##tag5" };
    auto tags = libzetk::extract_tags(input);
    ASSERT_EQ(tags, "#tag1 #2tag-2 #interr #tag3 #tag4 #tag5 ");
}

TEST(MdLinks, ImageNotLink)
{
    string input { "abc ![image](url) efg" };
    auto res = libzetk::extract_links(input);
    ASSERT_EQ(res, libzetk::LinkAndCountMap {});
}

TEST(MdLinks, UrlWithProtocol)
{
    string input { "abc ![https://www.example.com](url) efg" };
    auto res = libzetk::extract_links(input);
    ASSERT_EQ(res, libzetk::LinkAndCountMap {});
}

TEST(MdLinks, SingleLineLink)
{
    string input { "abc [title](url) efg" };
    auto res = libzetk::extract_links(input);

    libzetk::LinkAndCountMap expected_res { { "url", 1 } };
    ASSERT_EQ(expected_res, res);
}

TEST(MdLinks, LineStartLink)
{
    string input { "[title](url) efg" };
    auto res = libzetk::extract_links(input);

    libzetk::LinkAndCountMap expected_res { { "url", 1 } };
    ASSERT_EQ(expected_res, res);
}

TEST(MdLinks, MultiLineLink)
{
    string input { "abc [title](\nurl) efg" };
    auto res = libzetk::extract_links(input);

    libzetk::LinkAndCountMap expected_res { { "url", 1 } };
    ASSERT_EQ(expected_res, res);
}

TEST(ZkLinks, SingleLineLink)
{
    string input { "abc [[url]] efg" };
    auto res = libzetk::extract_links(input);

    libzetk::LinkAndCountMap expected_res { { "url", 1 } };
    ASSERT_EQ(expected_res, res);
}

TEST(ZkLinks, MultiLineLink)
{
    string input { "abc [[\nurl]] efg" };
    auto res = libzetk::extract_links(input);

    libzetk::LinkAndCountMap expected_res { { "url", 1 } };
    ASSERT_EQ(expected_res, res);
}
