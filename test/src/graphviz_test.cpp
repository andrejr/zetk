// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#include "zetk/db.hpp"
#include "zetk/graphviz.hpp"

#include <memory>

#include <gtest/gtest.h>

namespace libzetk {

void PrintTo(const FileIdsAndLinks &fal, std::ostream *os)
{
    *os << "{ .fileids = "s << testing::PrintToString(fal.fileids)
        << ", .links = "s << testing::PrintToString(fal.links) << " }"s;
}

void PrintTo(const TagsAndTagFileLinks &fal, std::ostream *os)
{
    *os << "{ .tags = "s << testing::PrintToString(fal.tags)
        << ", .fileids = "s << testing::PrintToString(fal.fileids)
        << ", .tag_links = "s << testing::PrintToString(fal.tag_links)
        << " }"s;
}

}

using namespace libzetk;
using SQLite::Database;
using std::string;
using std::stringstream;

class GraphvizTest : public ::testing::Test {
protected:
    void SetUp() override
    {
        db_ = std::make_unique<Database>((create_db(":memory:")));
    }

    std::unique_ptr<Database> db_;
};

TEST_F(GraphvizTest, JustLinks)
{
    // initial version
    add_file_to_db(filename_t("202005050837 What Ever.md"), "body1",
                   tag_t("#tag1 #tag2"), 1647335164, *db_);
    add_file_to_db(filename_t("202002020636 Blah Blah.md"), "body2",
                   tag_t("#tag1 #tag3"), 1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file4"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);

    add_or_update_links_for_file(
        filename_t("202005050837 What Ever.md"),
        { { "202002020636 Blah Blah.md", 2 }, { "file3", 2 } }, *db_);
    add_or_update_links_for_file(
        filename_t("202002020636 Blah Blah.md"),
        { { "202005050837 What Ever.md", 2 }, { "file4", 2 } }, *db_);

    std::string expected_code = R"(digraph {

  subgraph Files {
    node [ shape = rectangle ]

    2 [ label = "202002020636\nBlah Blah" ]
    1 [ label = "202005050837\nWhat Ever" ]
    3 [ label = "file3" ]
    4 [ label = "file4" ]
  }

  subgraph Links {
    1 -> 2
    1 -> 3
    2 -> 1
    2 -> 4
  }
}
)";

    std::stringstream ss;

    output_dot(false, ss, *db_);

    EXPECT_EQ(expected_code, ss.str());
}

TEST_F(GraphvizTest, LinksAndTags)
{
    // initial version
    add_file_to_db(filename_t("202005050837 What Ever.md"), "body1",
                   tag_t("#tag1 #tag2"), 1647335164, *db_);
    add_file_to_db(filename_t("202002020636 Blah Blah.md"), "body2",
                   tag_t("#tag1 #tag3"), 1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file4"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);

    add_or_update_links_for_file(
        filename_t("202005050837 What Ever.md"),
        { { "202002020636 Blah Blah.md", 2 }, { "file3", 2 } }, *db_);
    add_or_update_links_for_file(
        filename_t("202002020636 Blah Blah.md"),
        { { "202005050837 What Ever.md", 2 }, { "file4", 2 } }, *db_);
    std::string expected_code = R"(digraph {

  subgraph Files {
    node [ shape = rectangle ]

    2 [ label = "202002020636\nBlah Blah" ]
    1 [ label = "202005050837\nWhat Ever" ]
    3 [ label = "file3" ]
    4 [ label = "file4" ]
  }

  subgraph Links {
    1 -> 2
    1 -> 3
    2 -> 1
    2 -> 4
  }

  subgraph Tags {

    node [ shape = oval ]
    edge [ dir = none ]

    tag1 [ label = "#tag1" ]
    tag2 [ label = "#tag2" ]
    tag3 [ label = "#tag3" ]
    tag4 [ label = "#tag4" ]

    tag1 -> 1
    tag1 -> 2
    tag2 -> 1
    tag2 -> 3
    tag2 -> 4
    tag3 -> 2
    tag4 -> 3
    tag4 -> 4
  }
}
)";

    std::stringstream ss;

    output_dot(true, ss, *db_);

    EXPECT_EQ(expected_code, ss.str());
}

TEST_F(GraphvizTest, ConnectedFileidUnlimited)
{
    // initial version
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag1 #tag3"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file4"), "body3", tag_t("#tag2 #tag4"),
                   1647335167, *db_);
    add_file_to_db(filename_t("file5"), "body3", tag_t("#tag2 #tag4"),
                   1647335167, *db_);
    add_file_to_db(filename_t("file6"), "body3", tag_t("#tag2 #tag4"),
                   1647335167, *db_);

    add_or_update_links_for_file(filename_t("file1"),
                                 { { "file2", 2 }, { "file3", 2 } }, *db_);
    add_or_update_links_for_file(filename_t("file2"),
                                 { { "file1", 2 }, { "file4", 2 } }, *db_);
    add_or_update_links_for_file(filename_t("file5"), { { "file6", 8 } },
                                 *db_);

    FileIdsAndLinks expected_rowids {
        .fileids = { 3, 4, 2, 1 },
        .links = { { 2, 1 }, { 2, 4 }, { 1, 2 }, { 1, 3 } }
    };
    auto res = get_fileids_and_links_connected_to_file({ 1 }, *db_);

    EXPECT_EQ(expected_rowids, res);
}

TEST_F(GraphvizTest, ConnectedFileidLimited)
{
    // initial version
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag1 #tag3"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file4"), "body3", tag_t("#tag2 #tag4"),
                   1647335167, *db_);
    add_file_to_db(filename_t("file5"), "body3", tag_t("#tag2 #tag4"),
                   1647335167, *db_);
    add_file_to_db(filename_t("file6"), "body3", tag_t("#tag2 #tag4"),
                   1647335167, *db_);

    add_or_update_links_for_file(filename_t("file1"),
                                 { { "file2", 2 }, { "file3", 2 } }, *db_);
    add_or_update_links_for_file(filename_t("file2"),
                                 { { "file1", 2 }, { "file4", 2 } }, *db_);
    add_or_update_links_for_file(filename_t("file5"), { { "file6", 8 } },
                                 *db_);

    FileIdsAndLinks expected_rowids { .fileids = { 3, 2, 1 },
                                      .links = { { 1, 2 }, { 1, 3 } } };
    auto res = get_fileids_and_links_connected_to_file({ 1 }, *db_, 1);

    EXPECT_EQ(expected_rowids, res);
}

TEST_F(GraphvizTest, FileTagSiblings)
{
    // initial version
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag2 #tag3"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file4"), "body4", tag_t("#tag1 #tag4"),
                   1647335166, *db_);

    TagsAndTagFileLinks exp { .tags = { "tag3", "tag4", "tag1", "tag2" },
                              .fileids = { 4, 2, 3, 1 },
                              .tag_links = { { "tag3", 2 },
                                             { "tag4", 3 },
                                             { "tag4", 4 },
                                             { "tag1", 1 },
                                             { "tag1", 4 },
                                             { "tag2", 1 },
                                             { "tag2", 2 },
                                             { "tag2", 3 } } };

    auto res = get_tags_and_links_to_tags_from_files({ rowid_t { 1 } }, *db_);

    EXPECT_EQ(exp, res);
}

TEST_F(GraphvizTest, FileTagSiblingsTag)
{
    // initial version
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag2 #tag3"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file4"), "body4", tag_t("#tag1 #tag4"),
                   1647335166, *db_);

    TagsAndTagFileLinks exp { .tags = { "tag3", "tag4", "tag1", "tag2" },
                              .fileids = { 4, 2, 3, 1 },
                              .tag_links = { { "tag3", 2 },
                                             { "tag4", 3 },
                                             { "tag4", 4 },
                                             { "tag1", 1 },
                                             { "tag1", 4 },
                                             { "tag2", 1 },
                                             { "tag2", 2 },
                                             { "tag2", 3 } } };

    auto res =
        get_tags_and_links_to_tags_from_files({ tag_t { "tag1" } }, *db_);

    EXPECT_EQ(exp, res);
}

TEST_F(GraphvizTest, FileTagSiblingsTagLimited)
{
    // initial version
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag2 #tag3"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file4"), "body4", tag_t("#tag1 #tag4"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file5"), "body4", tag_t("#tag5"), 1647335166,
                   *db_);
    add_file_to_db(filename_t("file6"), "body4", tag_t("#tag5"), 1647335166,
                   *db_);

    TagsAndTagFileLinks exp { .tags = { "tag5" },
                              .fileids = { 5, 6 },
                              .tag_links = {
                                  { "tag5", 5 },
                                  { "tag5", 6 },
                              } };

    auto res =
        get_tags_and_links_to_tags_from_files({ tag_t { "tag5" } }, *db_);

    EXPECT_EQ(exp, res);
}

TEST_F(GraphvizTest, FileTagSiblingsDepth)
{
    // initial version
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag2 #tag3"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file4"), "body4", tag_t("#tag1 #tag4"),
                   1647335166, *db_);

    TagsAndTagFileLinks exp { .tags = { "tag1", "tag2" },
                              .fileids = { 1, 2, 3, 4 },
                              .tag_links = { { "tag1", 1 },
                                             { "tag1", 4 },
                                             { "tag2", 1 },
                                             { "tag2", 2 },
                                             { "tag2", 3 } } };

    auto res =
        get_tags_and_links_to_tags_from_files({ rowid_t { 1 } }, *db_, 1);

    EXPECT_EQ(exp, res);
}

TEST_F(GraphvizTest, JustLinksSubgraphFullDepth)
{
    // initial version
    add_file_to_db(filename_t("202005050837 What Ever.md"), "body1",
                   tag_t("#tag1 #tag2"), 1647335164, *db_);
    add_file_to_db(filename_t("202002020636 Blah Blah.md"), "body2",
                   tag_t("#tag1 #tag3"), 1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file4"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file5"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);

    add_or_update_links_for_file(
        filename_t("202005050837 What Ever.md"),
        { { "202002020636 Blah Blah.md", 2 }, { "file3", 2 } }, *db_);
    add_or_update_links_for_file(
        filename_t("202002020636 Blah Blah.md"),
        { { "202112050921 What Ever.md", 2 }, { "file4", 2 } }, *db_);

    std::string expected_code = R"(digraph {

  subgraph Files {
    node [ shape = rectangle ]

    2 [ label = "202002020636\nBlah Blah" ]
    1 [ label = "202005050837\nWhat Ever" ]
    3 [ label = "file3" ]
    4 [ label = "file4" ]
  }

  subgraph Links {
    2 -> 4
    1 -> 2
    1 -> 3
  }
}
)";

    std::stringstream ss;

    output_dot_subgraph({ rowid_t { 1 } }, false, ss, *db_);

    EXPECT_EQ(expected_code, ss.str());
}

TEST_F(GraphvizTest, JustLinksSubgraphLimitedDepth)
{
    // initial version
    add_file_to_db(filename_t("202005050837 What Ever.md"), "body1",
                   tag_t("#tag1 #tag2"), 1647335164, *db_);
    add_file_to_db(filename_t("202002020636 Blah Blah.md"), "body2",
                   tag_t("#tag1 #tag3"), 1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file4"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file5"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);

    add_or_update_links_for_file(
        filename_t("202005050837 What Ever.md"),
        { { "202002020636 Blah Blah.md", 2 }, { "file3", 2 } }, *db_);
    add_or_update_links_for_file(
        filename_t("202002020636 Blah Blah.md"),
        { { "202112050921 What Ever.md", 2 }, { "file4", 2 } }, *db_);

    std::string expected_code = R"(digraph {

  subgraph Files {
    node [ shape = rectangle ]

    2 [ label = "202002020636\nBlah Blah" ]
    1 [ label = "202005050837\nWhat Ever" ]
    3 [ label = "file3" ]
  }

  subgraph Links {
    1 -> 2
    1 -> 3
  }
}
)";

    std::stringstream ss;

    output_dot_subgraph({ rowid_t { 1 } }, false, ss, *db_, 1U);

    EXPECT_EQ(expected_code, ss.str());
}

TEST_F(GraphvizTest, LinksAndTagsSubgraphFullDepth)
{
    // initial version
    add_file_to_db(filename_t("202005050837 What Ever.md"), "body1",
                   tag_t("#tag1 #tag2"), 1647335164, *db_);
    add_file_to_db(filename_t("202002020636 Blah Blah.md"), "body2",
                   tag_t("#tag1 #tag3"), 1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file4"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file5"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file6"), "body3", tag_t(""), 1647335166, *db_);

    add_or_update_links_for_file(
        filename_t("202005050837 What Ever.md"),
        { { "202002020636 Blah Blah.md", 2 }, { "file3", 2 } }, *db_);
    add_or_update_links_for_file(
        filename_t("202002020636 Blah Blah.md"),
        { { "202112050921 What Ever.md", 2 }, { "file4", 2 } }, *db_);
    add_or_update_links_for_file(filename_t("file5"), { { "file6", 2 } },
                                 *db_);

    std::string expected_code = R"(digraph {

  subgraph Files {
    node [ shape = rectangle ]

    2 [ label = "202002020636\nBlah Blah" ]
    1 [ label = "202005050837\nWhat Ever" ]
    3 [ label = "file3" ]
    4 [ label = "file4" ]
    5 [ label = "file5" ]
  }

  subgraph Links {
    2 -> 4
    1 -> 2
    1 -> 3
  }

  subgraph Tags {

    node [ shape = oval ]
    edge [ dir = none ]

    tag3 [ label = "#tag3" ]
    tag4 [ label = "#tag4" ]
    tag1 [ label = "#tag1" ]
    tag2 [ label = "#tag2" ]

    tag3 -> 2
    tag4 -> 3
    tag4 -> 4
    tag4 -> 5
    tag1 -> 1
    tag1 -> 2
    tag2 -> 1
    tag2 -> 3
    tag2 -> 4
    tag2 -> 5
  }
}
)";

    std::stringstream ss;

    output_dot_subgraph({ rowid_t { 1 } }, true, ss, *db_);

    EXPECT_EQ(expected_code, ss.str());
}

TEST_F(GraphvizTest, LinksAndTagsSubgraphLimitedDepth)
{
    // initial version
    add_file_to_db(filename_t("202005050837 What Ever.md"), "body1",
                   tag_t("#tag1 #tag2"), 1647335164, *db_);
    add_file_to_db(filename_t("202002020636 Blah Blah.md"), "body2",
                   tag_t("#tag1 #tag3"), 1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file4"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file5"), "body3", tag_t("#tag2 #tag4 #tag1"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file6"), "body3", tag_t(""), 1647335166, *db_);

    add_or_update_links_for_file(
        filename_t("202005050837 What Ever.md"),
        { { "202002020636 Blah Blah.md", 2 }, { "file3", 2 } }, *db_);
    add_or_update_links_for_file(
        filename_t("202002020636 Blah Blah.md"),
        { { "202112050921 What Ever.md", 2 }, { "file4", 2 } }, *db_);
    add_or_update_links_for_file(filename_t("file5"), { { "file6", 2 } },
                                 *db_);

    std::string expected_code = R"(digraph {

  subgraph Files {
    node [ shape = rectangle ]

    2 [ label = "202002020636\nBlah Blah" ]
    1 [ label = "202005050837\nWhat Ever" ]
    3 [ label = "file3" ]
    4 [ label = "file4" ]
    5 [ label = "file5" ]
  }

  subgraph Links {
    1 -> 2
    1 -> 3
  }

  subgraph Tags {

    node [ shape = oval ]
    edge [ dir = none ]

    tag1 [ label = "#tag1" ]
    tag2 [ label = "#tag2" ]

    tag1 -> 1
    tag1 -> 2
    tag1 -> 5
    tag2 -> 1
    tag2 -> 3
    tag2 -> 4
    tag2 -> 5
  }
}
)";

    std::stringstream ss;

    output_dot_subgraph({ rowid_t { 1 } }, true, ss, *db_, 1);

    std::cout << ss.str();

    EXPECT_EQ(expected_code, ss.str());
}
