// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#include "zetk/cos_similarity.hpp"

#include <memory>

#include <gtest/gtest.h>

using namespace libzetk;
using std::vector;

TEST(CosSimTest, NonZero)
{
    vector<double> a { 1, 2, 1, 1, 1, 1, 0, 1, 0, 0,
                       0, 1, 1, 1, 0, 1, 1, 0, 0, 1 };
    vector<double> b { 1, 1, 0, 0, 1, 1, 1, 0, 1, 1,
                       1, 0, 1, 1, 1, 1, 0, 1, 1, 0 };
    auto result = cos_similarity_between_vectors(a, b);

    EXPECT_EQ(0.534522483824848793076967012893874198198318481445312, result);
}

TEST(CosSimTest, Zero)
{
    vector<double> a { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    vector<double> b { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    auto result = cos_similarity_between_vectors(a, b);

    EXPECT_EQ(0, result);
}

TEST(CosSimTest, Min)
{
    vector<double> a { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    vector<double> b { 0, 1, 0, 0, 0, 0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    auto result = cos_similarity_between_vectors(a, b);

    EXPECT_EQ(0, result);
}

TEST(CosSimTest, Max)
{
    vector<double> a { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    vector<double> b { 1, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                       0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    auto result = cos_similarity_between_vectors(a, b);

    EXPECT_EQ(1, result);
}

TEST(FilesSimilarToFile, Main)
{
    std::unordered_map<std::string, std::vector<double>>
        word_measures_per_file { { "file1", { 0, 2, 3 } },
                                 { "file2", { 0, 2, 2 } },
                                 { "file1clone", { 0, 2, 3 } },
                                 { "file3", { 3, 0, 0 } } };

    auto result = find_files_similar_to_file("file1", word_measures_per_file);
    SimilaritySet expected_result {
        { 1.00000000000000022204460492503130808472633361816406, "file1clone" },
        { 0.980580675690920000597827765886904671788215637207031, "file2" },
        { 0, "file3" }
    };

    EXPECT_EQ(expected_result, result);
}
