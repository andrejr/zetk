// Copyright © 2022 Andrej Radović
//
// This file is part of Zetk.
//
// Zetk is free software: you can redistribute it and/or modify it under the
// terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Zetk is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with Zetk. If not, see <https://www.gnu.org/licenses/>.

#include "zetk/db.hpp"

#include <memory>

#include <gtest/gtest.h>

using namespace libzetk;
using SQLite::Database;
using std::string;
using std::unordered_map;
using std::unordered_multimap;
using std::vector;

class DbTest : public ::testing::Test {
protected:
    void SetUp() override
    {
        db_ = std::make_unique<Database>((create_db(":memory:")));
    }

    std::unique_ptr<Database> db_;
};

TEST(CreationTest, Creation) { auto db = create_db(":memory:"); }

TEST_F(DbTest, AddAndListFiles)
{
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag-1 #tag2"),
                   1647335165, *db_);

    std::vector<std::pair<std::string, mtime_t>> expected_files {
        { "file1", 1647335164 }, { "file2", 1647335165 }
    };

    auto resulting_files = list_files_in_db(*db_);

    EXPECT_EQ(expected_files, resulting_files);
}

TEST_F(DbTest, ListFileIds)
{
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag-1 #tag2"),
                   1647335165, *db_);

    std::vector<std::pair<std::string, rowid_t>> expected_files {
        { "file1", 1 }, { "file2", 2 }
    };

    auto resulting_files = list_filenames_and_db_ids(*db_);

    EXPECT_EQ(expected_files, resulting_files);
}

TEST_F(DbTest, DeleteExistingFile)
{
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag-1 #tag2"),
                   1647335165, *db_);

    std::vector<std::pair<std::string, mtime_t>> expected_files {
        { "file2", 1647335165 }
    };

    delete_file_from_db(filename_t("file1"), *db_);
    auto resulting_files = list_files_in_db(*db_);

    EXPECT_EQ(expected_files, resulting_files);
}

TEST_F(DbTest, DeleteNonexistentFile)
{
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag-1 #tag2"),
                   1647335165, *db_);

    std::unordered_map<string, mtime_t> expected_files;
    expected_files["file1"] = 1647335164;
    expected_files["file2"] = 1647335165;

    delete_file_from_db(filename_t("no_existing_file"), *db_);
    auto resulting_files = get_files_in_db_map(*db_);

    EXPECT_EQ(expected_files, resulting_files);
}

TEST_F(DbTest, UpdateLinkOriginDoesntExist)
{
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag-1 #tag2"),
                   1647335165, *db_);

    LinkAndCountMap links = { { "file1", 1 }, { "file2", 2 } };
    EXPECT_THROW(
        {
            add_or_update_links_for_file(filename_t("nonexistent_file"), links,
                                         *db_);
        },
        not_found_error);
}

TEST_F(DbTest, UpdateLinkDestDoesntExist)
{
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag-1 #tag2"),
                   1647335165, *db_);

    LinkAndCountMap links = { { "file1", 1 }, { "nonexistent_file", 2 } };
}

void print_links(Database &db)
{
    SQLite::Statement query(
        db,
        "SELECT link_id, origin_id, dest_id, number_of_links\n"
        "   FROM links;\n");
    while (query.executeStep()) {
        std::cout << "id " << query.getColumn(0) << " origin "
                  << query.getColumn(1) << " dest " << query.getColumn(2)
                  << " nol " << query.getColumn(3) << "\n";
    }
}

TEST_F(DbTest, UpdateLinkListLinksOk)
{
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag-1 #tag2"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag-1 #tag2"),
                   1647335166, *db_);

    LinkAndCountMap links1 = { { "file2", 1 }, { "file3", 2 } };
    add_or_update_links_for_file(filename_t("file1"), links1, *db_);

    vector<std::pair<string, db_cnt_t>> expected_links1 { { "file2", 1 },
                                                          { "file3", 2 } };
    auto res1 = list_links_in_file(filename_t("file1"), *db_);
    EXPECT_EQ(expected_links1, res1);

    LinkAndCountMap links2 = { { "file2", 1 } };
    add_or_update_links_for_file(filename_t("file3"), links2, *db_);

    vector<std::pair<string, db_cnt_t>> expected_links2 { { "file2", 1 },
                                                          { "file3", 2 } };

    auto res2 = list_links_in_file(filename_t("file1"), *db_);

    EXPECT_EQ(expected_links2, res2);
}

TEST_F(DbTest, ListLinksNonexistent)
{
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag-1 #tag2"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag-1 #tag2"),
                   1647335166, *db_);

    LinkAndCountMap links = { { "file2", 1 }, { "file3", 2 } };
    add_or_update_links_for_file(filename_t("file1"), links, *db_);

    EXPECT_THROW({ list_links_in_file(filename_t("nonexistent_file"), *db_); },
                 not_found_error);
}

TEST_F(DbTest, ListLinksToOk)
{
    // initial version
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag-1 #tag2"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag-1 #tag2"),
                   1647335166, *db_);

    LinkAndCountMap links = { { "file2", 1 }, { "file3", 2 } };
    add_or_update_links_for_file(filename_t("file1"), links, *db_);

    vector<std::pair<string, db_cnt_t>> expected_links { { "file1", 1 } };

    auto res = list_files_linking_to_file(filename_t("file2"), *db_);

    EXPECT_EQ(expected_links, res);
}

TEST_F(DbTest, ListLinksToNonexistent)
{
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag-1 #tag2"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag-1 #tag2"),
                   1647335166, *db_);

    LinkAndCountMap links = { { "file2", 1 }, { "file3", 2 } };
    add_or_update_links_for_file(filename_t("file1"), links, *db_);

    EXPECT_THROW(
        { list_files_linking_to_file(filename_t("nonexistent_file"), *db_); },
        not_found_error);
}

TEST_F(DbTest, LinkCleanupOnFileDeletion)
{
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag-1 #tag2"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag-1 #tag2"),
                   1647335166, *db_);

    LinkAndCountMap links = { { "file2", 1 }, { "file3", 2 } };
    add_or_update_links_for_file(filename_t("file1"), links, *db_);

    delete_file_from_db(filename_t("file1"), *db_);

    vector<std::pair<string, db_cnt_t>> expected_links;

    auto res = list_files_linking_to_file(filename_t("file2"), *db_);

    EXPECT_EQ(expected_links, res);
}

TEST_F(DbTest, GlobalFts)
{
    string lorem1 =
        "Vestibulum faucibus, arcu ac luctus congue, ex ligula auctor augue, "
        "tristique sollicitudin magna orci mollis leo. Pellentesque ac diam "
        "mattis, tempus mauris at, scelerisque lectus. Sed pharetra id urna "
        "et consectetur. Vestibulum quis porttitor tortor, dignissim cursus "
        "dui. Etiam ornare sit amet ligula vel posuere. Pellentesque nec "
        "congue erat. Quisque auctor, libero et porttitor vestibulum, nisl "
        "ligula lobortis enim, euismod sodales purus turpis id nulla. Proin "
        "mattis ipsum ac commodo viverra. #tag-1 #tag2";

    string lorem2 =
        "Aenean at faucibus libero. Etiam et augue tellus. Maecenas posuere "
        "euismod tincidunt. Sed aliquet porta neque nec mattis. Vestibulum "
        "lectus massa, tempor quis tincidunt at, ultricies non risus. Duis "
        "bibendum orci lacus, nec ultrices nibh ultricies nec. Nullam "
        "fermentum elit mauris, non dictum velit ultrices a. #tag3 #tag2";

    add_file_to_db(filename_t("file1"), lorem1, tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), lorem2, tag_t("#tag3 #tag2"),
                   1647335165, *db_);

    auto result = fulltext_search_all_files("tellus", *db_);

    vector<FtsResult> expected = { FtsResult {
        "file2", lorem2, "#tag3 #tag2",
        -1.08758271701050983693080491388e-06 } };

    EXPECT_EQ(expected, result);
}

TEST_F(DbTest, InFileFts)
{
    string lorem1 =
        "Vestibulum faucibus, arcu ac luctus congue, ex ligula auctor augue, "
        "tristique sollicitudin magna orci mollis leo. Pellentesque ac diam "
        "mattis, tempus mauris at, scelerisque lectus. Sed pharetra id urna "
        "et consectetur. Vestibulum quis porttitor tortor, dignissim cursus "
        "dui. Etiam ornare sit amet ligula vel posuere. Pellentesque nec "
        "congue erat. Quisque auctor, libero et porttitor vestibulum, nisl "
        "ligula lobortis enim, euismod sodales purus turpis id nulla. Proin "
        "mattis ipsum ac commodo viverra. #tag-1 #tag2";

    string lorem2 =
        "Aenean at faucibus libero. Etiam et augue tellus. Maecenas posuere "
        "euismod tincidunt. Sed aliquet porta neque nec mattis. Vestibulum "
        "lectus massa, tempor quis tincidunt at, ultricies non risus. Duis "
        "bibendum orci lacus, nec ultrices nibh ultricies nec. Nullam "
        "fermentum elit mauris, non dictum velit ultrices a. #tag3 #tag2";

    add_file_to_db(filename_t("file1"), lorem1, tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), lorem2, tag_t("#tag3 #tag2"),
                   1647335165, *db_);

    auto result = fulltext_search_in_file("augue", filename_t("file2"), *db_);

    std::optional<FtsResult> expected = { FtsResult {
        "file2", lorem2, "#tag3 #tag2",
        -1.08758271701050983693080491388e-06 } };

    EXPECT_EQ(expected, result);
}

TEST_F(DbTest, VocabAllWords)
{
    string lorem1 = "Vitae dolores nemo porro quisquam ut. Consequatur "
                    "similique at architecto sed cumque. #tag-1 #tag2";

    string lorem2 = "Consequatur similique at architecto sed cumque. Est aut "
                    "beatae voluptatem saepe."
                    "#tag3 #tag2";

    add_file_to_db(filename_t("file1"), lorem1, tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), lorem2, tag_t("#tag3 #tag2"),
                   1647335165, *db_);

    auto result = get_vocab_words(*db_);
    vector<string> expected_result = {
        "architecto", "at",      "aut", "beatae",    "consequatur",
        "cumque",     "dolores", "est", "nemo",      "porro",
        "quisquam",   "saepe",   "sed", "similique", "tag-1",
        "tag2",       "tag3",    "ut",  "vitae",     "voluptatem"
    };

    EXPECT_EQ(expected_result, result);
}

TEST_F(DbTest, VocabAllWordsWithStopWords)
{
    string lorem1 = "Vitae dolores nemo porro quisquam ut. Consequatur "
                    "similique at architecto sed cumque. #tag-1 #tag2";

    string lorem2 = "Consequatur similique at architecto sed cumque. Est aut "
                    "beatae voluptatem saepe."
                    "#tag3 #tag2";

    std::unordered_set<string> stop_words { "porro" };

    add_file_to_db(filename_t("file1"), lorem1, tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), lorem2, tag_t("#tag3 #tag2"),
                   1647335165, *db_);

    auto result = get_vocab_words(*db_, stop_words);
    vector<string> expected_result = {
        "architecto", "at",      "aut",       "beatae",    "consequatur",
        "cumque",     "dolores", "est",       "nemo",      "quisquam",
        "saepe",      "sed",     "similique", "tag-1",     "tag2",
        "tag3",       "ut",      "vitae",     "voluptatem"
    };

    EXPECT_EQ(expected_result, result);
}

TEST_F(DbTest, WordFrequenciesPerFile)
{
    string lorem1 = "Vitae dolores nemo porro quisquam ut. Consequatur "
                    "similique at architecto sed cumque. #tag-1 #tag2";

    string lorem2 = "Consequatur similique at architecto sed cumque. Est aut "
                    "beatae at voluptatem saepe."
                    "#tag3 #tag2";

    add_file_to_db(filename_t("file1"), lorem1, tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), lorem2, tag_t("#tag3 #tag2"),
                   1647335165, *db_);

    unordered_map<string, vector<rank_t>> expected_result {
        { "file2",
          { 1, 2, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 1 } },
        { "file1",
          { 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0 } }
    };
    auto result = get_word_frequencies_per_file(*db_);

    EXPECT_EQ(expected_result, result);
}

TEST_F(DbTest, TagIncidencePerFile)
{
    string lorem1 = "Vitae dolores nemo porro quisquam ut. Consequatur "
                    "similique at architecto sed cumque. #tag-1 #tag2";

    string lorem2 = "Consequatur similique at architecto sed cumque. Est aut "
                    "beatae at voluptatem saepe."
                    "#tag3 #tag2";

    add_file_to_db(filename_t("file1"), lorem1, tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), lorem2, tag_t("#tag3 #tag2"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), lorem2, tag_t("#tag3 #tag4"),
                   1647335165, *db_);

    unordered_map<string, vector<rank_t>> expected_result {
        { "file3", { 0, 0, 1, 1 } },
        { "file2", { 0, 1, 1, 0 } },
        { "file1", { 1, 1, 0, 0 } }
    };
    auto result = get_tag_incidence_per_file(*db_);

    EXPECT_EQ(expected_result, result);
}

TEST_F(DbTest, UniqueFileIdExact)
{
    add_file_to_db(filename_t("file1"), "abc", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "abc", tag_t("#tag3 #tag2"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "abc", tag_t("#tag3 #tag4"),
                   1647335165, *db_);

    std::optional<rowid_t> expected_result { 1 };
    auto result = get_unique_file_id_from_prefix(filename_t { "file1" }, *db_);

    EXPECT_EQ(expected_result, result);
}

TEST_F(DbTest, UniqueFileIdInexact)
{
    add_file_to_db(filename_t("abcdefg"), "abc", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "abc", tag_t("#tag3 #tag2"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "abc", tag_t("#tag3 #tag4"),
                   1647335165, *db_);

    std::optional<rowid_t> expected_result { 1 };
    auto result = get_unique_file_id_from_prefix(filename_t { "abcd" }, *db_);

    EXPECT_EQ(expected_result, result);
}

TEST_F(DbTest, UniqueFileIdAmbiguous)
{
    add_file_to_db(filename_t("file1"), "abc", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "abc", tag_t("#tag3 #tag2"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "abc", tag_t("#tag3 #tag4"),
                   1647335165, *db_);

    std::optional<rowid_t> expected_result {};
    auto result = get_unique_file_id_from_prefix(filename_t { "file" }, *db_);

    EXPECT_EQ(expected_result, result);
}

TEST_F(DbTest, UniqueFileIdNonexistant)
{
    add_file_to_db(filename_t("file1"), "abc", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "abc", tag_t("#tag3 #tag2"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "abc", tag_t("#tag3 #tag4"),
                   1647335165, *db_);

    std::optional<rowid_t> expected_result {};
    auto result = get_unique_file_id_from_prefix(filename_t { "xyzzy" }, *db_);

    EXPECT_EQ(expected_result, result);
}

TEST_F(DbTest, UniqueFilenameExact)
{
    add_file_to_db(filename_t("file1"), "abc", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "abc", tag_t("#tag3 #tag2"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "abc", tag_t("#tag3 #tag4"),
                   1647335165, *db_);

    std::optional<std::string> expected_result { "file1" };
    auto result =
        get_unique_filename_from_prefix(filename_t { "file1" }, *db_);

    EXPECT_EQ(expected_result, result);
}

TEST_F(DbTest, UniqueFilenameInexact)
{
    add_file_to_db(filename_t("abcdefg"), "abc", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "abc", tag_t("#tag3 #tag2"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "abc", tag_t("#tag3 #tag4"),
                   1647335165, *db_);

    std::optional<std::string> expected_result { "abcdefg" };
    auto result = get_unique_filename_from_prefix(filename_t { "abcd" }, *db_);

    EXPECT_EQ(expected_result, result);
}

TEST_F(DbTest, UniqueFilenameAmbiguous)
{
    add_file_to_db(filename_t("file1"), "abc", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "abc", tag_t("#tag3 #tag2"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "abc", tag_t("#tag3 #tag4"),
                   1647335165, *db_);

    std::optional<std::string> expected_result {};
    auto result = get_unique_filename_from_prefix(filename_t { "file" }, *db_);

    EXPECT_EQ(expected_result, result);
}

TEST_F(DbTest, UniqueFilenameNonexistant)
{
    add_file_to_db(filename_t("file1"), "abc", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "abc", tag_t("#tag3 #tag2"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "abc", tag_t("#tag3 #tag4"),
                   1647335165, *db_);

    std::optional<std::string> expected_result {};
    auto result =
        get_unique_filename_from_prefix(filename_t { "xyzzy" }, *db_);

    EXPECT_EQ(expected_result, result);
}

TEST_F(DbTest, WordFrequenciesPerFileWithStopWords)
{
    string lorem1 = "Vitae dolores nemo porro quisquam ut. Consequatur "
                    "similique at architecto sed cumque. #tag-1 #tag2";

    string lorem2 = "Consequatur similique at architecto sed cumque. Est aut "
                    "beatae at voluptatem saepe."
                    "#tag3 #tag2";

    std::unordered_set<string> stop_words { "porro" };

    add_file_to_db(filename_t("file1"), lorem1, tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), lorem2, tag_t("#tag3 #tag2"),
                   1647335165, *db_);

    unordered_map<string, vector<rank_t>> expected_result {
        { "file2",
          { 1, 2, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 1 } },
        { "file1",
          { 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0 } }
    };

    auto result = get_word_frequencies_per_file(*db_, stop_words);

    EXPECT_EQ(expected_result, result);
}

TEST_F(DbTest, FileCountsPerWord)
{
    string lorem1 = "Vitae dolores nemo porro quisquam ut. Consequatur "
                    "similique at architecto sed cumque. #tag-1 #tag2";

    string lorem2 = "Consequatur similique at architecto sed cumque. Est aut "
                    "beatae at voluptatem saepe."
                    "#tag3 #tag2";

    add_file_to_db(filename_t("file1"), lorem1, tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), lorem2, tag_t("#tag3 #tag2"),
                   1647335165, *db_);

    unordered_map<string, rank_t> expected_result {
        { "tag2", 2 },       { "tag-1", 1 },       { "tag3", 1 },
        { "similique", 2 },  { "architecto", 2 },  { "quisquam", 1 },
        { "voluptatem", 1 }, { "vitae", 1 },       { "aut", 1 },
        { "beatae", 1 },     { "est", 1 },         { "ut", 1 },
        { "at", 2 },         { "consequatur", 2 }, { "cumque", 2 },
        { "dolores", 1 },    { "sed", 2 },         { "nemo", 1 },
        { "porro", 1 },      { "saepe", 1 }
    };

    auto result = get_file_counts_per_word(*db_);

    EXPECT_EQ(expected_result, result);
}

TEST_F(DbTest, FileCountsPerWordWithStopWords)
{
    string lorem1 = "Vitae dolores nemo porro quisquam ut. Consequatur "
                    "similique at architecto sed cumque. #tag-1 #tag2";

    string lorem2 = "Consequatur similique at architecto sed cumque. Est aut "
                    "beatae at voluptatem saepe."
                    "#tag3 #tag2";

    std::unordered_set<string> stop_words { "porro" };

    add_file_to_db(filename_t("file1"), lorem1, tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), lorem2, tag_t("#tag3 #tag2"),
                   1647335165, *db_);

    unordered_map<string, rank_t> expected_result {
        { "tag2", 2 },     { "tag-1", 1 },      { "architecto", 2 },
        { "quisquam", 1 }, { "voluptatem", 1 }, { "vitae", 1 },
        { "aut", 1 },      { "beatae", 1 },     { "est", 1 },
        { "ut", 1 },       { "at", 2 },         { "consequatur", 2 },
        { "cumque", 2 },   { "dolores", 1 },    { "sed", 2 },
        { "nemo", 1 },     { "saepe", 1 },      { "tag3", 1 },
        { "similique", 2 }
    };

    auto result = get_file_counts_per_word(*db_, stop_words);

    EXPECT_EQ(expected_result, result);
}

TEST_F(DbTest, TfIdfPerFile)
{
    string lorem1 = "Vitae dolores nemo porro quisquam ut. Consequatur "
                    "similique at architecto sed cumque. #tag-1 #tag2";

    string lorem2 = "Consequatur similique at architecto sed cumque. Est aut "
                    "beatae at voluptatem saepe."
                    "#tag3 #tag2";

    add_file_to_db(filename_t("file1"), lorem1, tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), lorem2, tag_t("#tag3 #tag2"),
                   1647335165, *db_);

    unordered_map<string, vector<rank_t>> expected_result {
        { "file2",
          { 0,
            0,
            0.693147180559945286226763982995180413126945495605469,
            0.693147180559945286226763982995180413126945495605469,
            0,
            0,
            0,
            0.693147180559945286226763982995180413126945495605469,
            0,
            0,
            0,
            0.693147180559945286226763982995180413126945495605469,
            0,
            0,
            0,
            0,
            0.693147180559945286226763982995180413126945495605469,
            0,
            0,
            0.693147180559945286226763982995180413126945495605469 } },
        { "file1",
          { 0,
            0,
            0,
            0,
            0,
            0,
            0.693147180559945286226763982995180413126945495605469,
            0,
            0.693147180559945286226763982995180413126945495605469,
            0.693147180559945286226763982995180413126945495605469,
            0.693147180559945286226763982995180413126945495605469,
            0,
            0,
            0,
            0.693147180559945286226763982995180413126945495605469,
            0,
            0,
            0.693147180559945286226763982995180413126945495605469,
            0.693147180559945286226763982995180413126945495605469,
            0 } }
    };
    auto result = get_tf_idf_per_file(*db_);

    /* std::cout << std::setprecision(51) << result["file2"][2]; */
    EXPECT_EQ(expected_result, result);
}

TEST_F(DbTest, TfIdfPerFileWithStopWords)
{
    string lorem1 = "Vitae dolores nemo porro quisquam ut. Consequatur "
                    "similique at architecto sed cumque. #tag-1 #tag2";

    string lorem2 = "Consequatur similique at architecto sed cumque. Est aut "
                    "beatae at voluptatem saepe."
                    "#tag3 #tag2";

    std::unordered_set<string> stop_words { "porro" };

    add_file_to_db(filename_t("file1"), lorem1, tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), lorem2, tag_t("#tag3 #tag2"),
                   1647335165, *db_);

    unordered_map<string, vector<rank_t>> expected_result {
        { "file2",
          { 0, 0, 0.693147180559945286226763982995180413126945495605469,
            0.693147180559945286226763982995180413126945495605469, 0, 0, 0,
            0.693147180559945286226763982995180413126945495605469, 0, 0,
            0.693147180559945286226763982995180413126945495605469, 0, 0, 0, 0,
            0.693147180559945286226763982995180413126945495605469, 0, 0,
            0.693147180559945286226763982995180413126945495605469 } },
        { "file1",
          { 0, 0, 0, 0, 0, 0,
            0.693147180559945286226763982995180413126945495605469, 0,
            0.693147180559945286226763982995180413126945495605469,
            0.693147180559945286226763982995180413126945495605469, 0, 0, 0,
            0.693147180559945286226763982995180413126945495605469, 0, 0,
            0.693147180559945286226763982995180413126945495605469,
            0.693147180559945286226763982995180413126945495605469, 0 } }
    };

    auto result = get_tf_idf_per_file(*db_, stop_words);

    EXPECT_EQ(expected_result, result);
}

TEST_F(DbTest, ListLinksAsIds)
{
    // initial version
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag-1 #tag2"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag-1 #tag2"),
                   1647335166, *db_);

    LinkAndCountMap links = { { "file2", 1 }, { "file3", 2 } };
    add_or_update_links_for_file(filename_t("file1"), links, *db_);

    std::vector<std::pair<rowid_t, rowid_t>> expected_links { { 1, 2 },
                                                              { 1, 3 } };

    auto res = get_links_as_id_pairs(*db_);

    EXPECT_EQ(expected_links, res);
}

TEST_F(DbTest, ListLinksAsIdUmmap)
{
    // initial version
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag-1 #tag2"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag-1 #tag2"),
                   1647335166, *db_);

    LinkAndCountMap links = { { "file2", 1 }, { "file3", 2 } };
    add_or_update_links_for_file(filename_t("file1"), links, *db_);

    std::unordered_multimap<rowid_t, rowid_t> expected_links { { 1, 2 },
                                                               { 1, 3 } };

    auto res = get_links_as_id_ummap(*db_);

    EXPECT_EQ(expected_links, res);
}

TEST_F(DbTest, ListLinksAsFilenames)
{
    // initial version
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag-1 #tag2"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag-1 #tag2"),
                   1647335166, *db_);

    LinkAndCountMap links = { { "file2", 1 }, { "file3", 2 } };
    add_or_update_links_for_file(filename_t("file1"), links, *db_);

    std::vector<std::pair<std::string, std::string>> expected_links {
        { "file1", "file2" }, { "file1", "file3" }
    };

    auto res = get_links_as_filename_pairs(*db_);

    EXPECT_EQ(expected_links, res);
}

TEST_F(DbTest, GetCoexistingTags)
{
    // initial version
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag-1 #tag3"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);

    std::vector<std::string> expected_tags { "tag2", "tag3" };

    auto res = get_coexisting_tags({ tag_t("tag-1") }, *db_);

    EXPECT_EQ(expected_tags, res);
}

TEST_F(DbTest, GetCoexistingTagsMultiple)
{
    // initial version
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag-1 #tag3"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);

    std::vector<std::string> expected_tags { "tag3", "tag4" };

    auto res = get_coexisting_tags({ tag_t("tag-1"), tag_t("tag2") }, *db_);

    EXPECT_EQ(expected_tags, res);
}

TEST_F(DbTest, GetCoexistingTagsZero)
{
    // initial version
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag-1 #tag3"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);

    std::vector<std::string> expected_tags {};

    EXPECT_THROW({ get_coexisting_tags({}, *db_); }, argument_error);
}

TEST_F(DbTest, FileLinkTreeUnlimited)
{
    // initial version
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag-1 #tag3"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file4"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);

    LinkAndCountMap links1 = { { "file2", 2 }, { "file3", 2 } };
    add_or_update_links_for_file(filename_t("file1"), links1, *db_);
    LinkAndCountMap links2 = { { "file1", 2 }, { "file4", 2 } };
    add_or_update_links_for_file(filename_t("file2"), links2, *db_);

    std::vector<std::pair<db_cnt_t, std::string>> expected_tree {
        { 0, "file1" }, { 1, "file3" }, { 1, "file2" }, { 2, "file4" }
    };

    auto res = get_file_link_tree(filename_t("file1"), *db_, 0);

    EXPECT_EQ(expected_tree, res);
}

TEST_F(DbTest, FileLinkTreeLimitedDepth)
{
    // initial version
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag-1 #tag3"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file4"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);

    LinkAndCountMap links1 = { { "file2", 2 }, { "file3", 2 } };
    add_or_update_links_for_file(filename_t("file1"), links1, *db_);
    LinkAndCountMap links2 = { { "file1", 2 }, { "file4", 2 } };
    add_or_update_links_for_file(filename_t("file2"), links2, *db_);

    std::vector<std::pair<db_cnt_t, std::string>> expected_tree {
        { 0, "file1" }, { 1, "file3" }, { 1, "file2" }
    };

    auto res = get_file_link_tree(filename_t("file1"), *db_, 1);

    EXPECT_EQ(expected_tree, res);
}

TEST_F(DbTest, FileLinkTreeEmpty)
{
    // initial version
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag-1 #tag3"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file4"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);

    LinkAndCountMap links1 = { { "file1", 2 }, { "file2", 2 } };
    add_or_update_links_for_file(filename_t("file1"), links1, *db_);
    LinkAndCountMap links2 = { { "file1", 2 } };
    add_or_update_links_for_file(filename_t("file2"), links2, *db_);

    std::vector<std::pair<db_cnt_t, std::string>> expected_tree {
        { 0, "file3" }
    };

    auto res = get_file_link_tree(filename_t("file3"), *db_);

    EXPECT_EQ(expected_tree, res);
}

TEST_F(DbTest, TagsInFile)
{
    // initial version
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag2 #tag3"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file4"), "body4", tag_t("#tag-1 #tag4"),
                   1647335166, *db_);

    std::vector<std::string> expected_tags { "tag-1", "tag2" };

    auto res = list_tags_in_file(filename_t("file1"s), *db_);

    EXPECT_EQ(expected_tags, res);
}

TEST_F(DbTest, FilesContainingTags)
{
    // initial version
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag-1 #tag3"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);

    std::vector<std::string> expected_files { "file1", "file2" };

    auto res = list_files_containing_tag(tag_t("tag-1"), *db_);

    EXPECT_EQ(expected_files, res);
}

TEST_F(DbTest, FileTagSiblingTree)
{
    // initial version
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag2 #tag3"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file4"), "body4", tag_t("#tag-1 #tag4"),
                   1647335166, *db_);

    std::vector<std::pair<db_cnt_t, std::string>> expected_tree {
        { 0, "file1" }, { 1, "#tag-1" }, { 2, "file1" }, { 2, "file4" },
        { 3, "#tag4" }, { 4, "file3" },  { 4, "file4" }, { 1, "#tag2" },
        { 2, "file1" }, { 2, "file2" },  { 3, "#tag3" }, { 4, "file2" },
        { 2, "file3" }
    };

    auto res = get_file_tag_sibling_tree(filename_t("file1"), *db_);

    EXPECT_EQ(expected_tree, res);
}

TEST_F(DbTest, FileTagSiblingTreeLimitedDepth)
{
    // initial version
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag2 #tag3"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file4"), "body4", tag_t("#tag-1 #tag4"),
                   1647335166, *db_);

    std::vector<std::pair<db_cnt_t, std::string>> expected_tree {
        { 0, "file1" }, { 1, "#tag-1" }, { 2, "file1" }, { 2, "file4" },
        { 1, "#tag2" }, { 2, "file1" },  { 2, "file2" }, { 2, "file3" }
    };

    auto res = get_file_tag_sibling_tree(filename_t("file1"), *db_, 1);

    EXPECT_EQ(expected_tree, res);
}

TEST_F(DbTest, GetExistingFileFromDb)
{
    // initial version
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag2 #tag3"),
                   1647335165, *db_);

    std::optional<std::string> expected_res = { "body1" };
    auto res = get_file_contents_from_db(filename_t("file1"), *db_);

    EXPECT_EQ(expected_res, res);
}

TEST_F(DbTest, GetNonExistingFileFromDb)
{
    // initial version
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag2 #tag3"),
                   1647335165, *db_);

    std::optional<std::string> expected_res = {};
    auto res = get_file_contents_from_db(filename_t("file3"), *db_);

    EXPECT_EQ(expected_res, res);
}

TEST_F(DbTest, GetExistingFileMetadataFromDb)
{
    // initial version
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag2 #tag3"),
                   1647335165, *db_);

    std::optional<FileMetadata> expected_res = {
        { .title = "file1", .id = 1, .mtime = 1647335164 }
    };
    auto res = get_file_metadata(filename_t("file1"), *db_);

    EXPECT_EQ(expected_res, res);
}

TEST_F(DbTest, GetNonExistingFileMetadataFromDb)
{
    // initial version
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag2 #tag3"),
                   1647335165, *db_);

    std::optional<FileMetadata> expected_res = {};
    auto res = get_file_metadata(filename_t("file3"), *db_);

    EXPECT_EQ(expected_res, res);
}

TEST_F(DbTest, TagFileidPairs)
{
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag2"), 1647335165,
                   *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag-1 #tag3"),
                   1647335166, *db_);

    vector<std::pair<string, rowid_t>> expected_pairs { { "tag-1", 1 },
                                                        { "tag-1", 3 },
                                                        { "tag2", 1 },
                                                        { "tag2", 2 },
                                                        { "tag3", 3 } };

    auto res = get_tag_fileid_pairs(*db_);

    EXPECT_EQ(expected_pairs, res);
}

TEST_F(DbTest, TagFileidUmmap)
{
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag2 #tag3"),
                   1647335165, *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag2 #tag4"),
                   1647335166, *db_);
    add_file_to_db(filename_t("file4"), "body4", tag_t("#tag-1 #tag4"),
                   1647335166, *db_);

    std::unordered_multimap<string, rowid_t> expected_pairs {
        { "tag4", 4 }, { "tag4", 3 }, { "tag3", 2 },  { "tag2", 3 },
        { "tag2", 2 }, { "tag2", 1 }, { "tag-1", 4 }, { "tag-1", 1 }
    };

    auto res = get_tag_fileid_ummap(*db_);

    EXPECT_EQ(expected_pairs, res);
}

TEST_F(DbTest, FileidTagUmmap)
{
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag2"), 1647335165,
                   *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag-1 #tag3"),
                   1647335166, *db_);

    std::unordered_multimap<rowid_t, string> expected_pairs { { 2, "tag2" },
                                                              { 3, "tag3" },
                                                              { 3, "tag-1" },
                                                              { 1, "tag2" },
                                                              { 1, "tag-1" } };

    auto res = get_fileid_tag_ummap(*db_);

    EXPECT_EQ(expected_pairs, res);
}

TEST_F(DbTest, ListTags)
{
    add_file_to_db(filename_t("file1"), "body1", tag_t("#tag-1 #tag2"),
                   1647335164, *db_);
    add_file_to_db(filename_t("file2"), "body2", tag_t("#tag2"), 1647335165,
                   *db_);
    add_file_to_db(filename_t("file3"), "body3", tag_t("#tag-1 #tag3"),
                   1647335166, *db_);

    vector<std::string> expected_tags { "tag-1", "tag2", "tag3" };

    auto res = list_all_tags(*db_);

    EXPECT_EQ(expected_tags, res);
}
